# Informationstechnologien

Dokumentation zum Fach *Informationstechnologien* der HFMT, Klasse 19A und Klasse 21A Herbstsemester 2019 und 2021.

## Lizenzierung

* Procrammcode: MIT, siehe [LICENSE](LICENSE)
* Inhalt: CC BY-NC-SA, siehe [Dokumentation Version 2.X, Lizenzierung](https://informationstechnologien.readthedocs.io/de/latest/pages/index.html#lizenzierung)
* Drittprodukte/Open Source Software: siehe [Dokumentation Version 2.X, Komponenten](https://informationstechnologien.readthedocs.io/de/latest/pages/index.html#komponenten)

## Versionen

### Aktiv

[![Documentation Status](https://readthedocs.org/projects/informationstechnologien/badge/?version=latest)](https://informationstechnologien.readthedocs.io/de/latest/?badge=latest)

* Version 2.X, siehe [informationstechnologien.readthedocs.io](https://informationstechnologien.readthedocs.io/de/latest/)

* Syntax: [MyST Markdown](https://jupyterbook.org/reference/cheatsheet.html) 

### Veraltet (abgeschaltet)

* Version 1.X, siehe [meengit.gitlab.io/hfmt19ict/](https://meengit.gitlab.io/hfmt19ict/)

## Impressum

**Kontakt-Adresse**

ansi labs GmbH<br>
Winkelstrasse 34<br>
8706 Meilen<br>
Schweiz

E-Mail:<br>
<hfmt.ict@meen.ch>

**Vertretungsberechtigte Person(en)**

Andreas Eberhard, Gesellschafter<br>
Sinan Güder, Gesellschafter

**Haftungsausschluss**

Der Autor übernimmt keinerlei Gewähr hinsichtlich der inhaltlichen Richtigkeit, Genauigkeit, Aktualität, Zuverlässigkeit und Vollständigkeit der Informationen.

Haftungsansprüche gegen den Autor wegen Schäden materieller oder immaterieller Art, welche aus dem Zugriff oder der Nutzung bzw. Nichtnutzung der veröffentlichten Informationen, durch Missbrauch der Verbindung oder durch technische Störungen entstanden sind, werden ausgeschlossen.

Alle Angebote sind unverbindlich. Der Autor behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne besondere Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.

**Haftungsausschluss für Links**

Verweise und Links auf Webseiten Dritter liegen ausserhalb unseres Verantwortungsbereichs. Es wird jegliche Verantwortung für solche Webseiten abgelehnt. Der Zugriff und die Nutzung solcher Webseiten erfolgen auf eigene Gefahr des jeweiligen Nutzers.

**Urheberrechte**

Die Urheber- und alle anderen Rechte an Inhalten, Bildern, Fotos oder anderen Dateien auf dieser Website, gehören ausschliesslich **der juristischen Person ansi labs GmbH** oder den speziell genannten Rechteinhabern. Für die Reproduktion jeglicher Elemente ist die schriftliche Zustimmung des Urheberrechtsträgers im Voraus einzuholen.

*Quelle: <a href="https://www.swissanwalt.ch" rel="noopener" target="_blank">SwissAnwalt</a>*

## Datenschutz

Wir verarbeiten keine Besucherdaten – insbesondere wird der Inhalt bis auf Weiteres ***nicht*** mit Google Analytics überwacht. Möglicherweise werden personenbezogene Daten durch die Plattform [readthedocs.org](https://readthedocs.org) erhoben. Lesen sie dazu bitte die [Privacy Policy von Read the Docs](https://docs.readthedocs.io/en/stable/privacy-policy.html).
