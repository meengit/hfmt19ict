.. Grundlagen Informationstechnologien documentation master file, created by
   sphinx-quickstart on Wed Apr  7 05:45:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Grundlagen Informationstechnologien
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/index.md
   pages/einleitendes.md
   pages/hardware.md
   pages/netzwerk.md
   pages/cms.md
   pages/services.md
   pages/projektmanagement.md
   pages/design.md
   pages/datenbanken.md
   pages/markdown.md
   pages/quellen.md
   pages/bibliografie.md

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
