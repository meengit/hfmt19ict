# Content Management

```{warning}
Dieser Inhalt ist noch nicht abschliessend redigiert! Er enthält möglicherweise Fehler.
```

## Übersicht

Medien produzieren Inhalte und die Summe dieser Inhalte wird in der digitalen Medienproduktion gerne als Content bezeichnet. Für die Strukturierung von Content gibt es eine heute eine Reihe von Programmen. Sie werden gerne als Content-Management-Systeme, kurz CMS, bezeichnet. Ein CMS ist in der Regel ein ganzes Softwarepaket, das möglichst viele Aufgaben aus dem Workflow übernehmen soll. Es hilft zum Beispiel beim Erstellen von Inhalten, bei deren Verwaltung und bei deren Administration ({cite:ps}`bohringer_kompendium_2008`, S. 736).

Es gibt eine ganze Reihe von Programmen für das Management von Content – und das mit verschiedenen Ausprägungen. Sie werden deshalb gerne in Kategorien eingeteilt. Zum Beispiel:

- Media-Asset-Management(-System, kurz MAM)
- Database Publishing
- Digital-Asset-Management(-System, kurz DAM)

Die Kategorien lassen sich aber nicht haarscharf abgrenzen. Oft gibt es Überlappungen, wo ein Programm in die eine oder andere Richtung etwas mehr oder weniger kann.

## CMS

Für den Augenblick bleiben wir beim Content-Management-System oder CMS. Ein CMS steht fast immer im Zusammenhang mit dem Internet. Es «managed» die Inhalte für Autoren und Lesende. Für die Lesenden wird der Inhalt «on demand» zusammengestellt, je nachdem, welchen Inhalt sie aufrufen – zum Beispiel auf einer Webseite. Für die Autoren stellt es eine Reihe von Werkzeugen bereit, die ihnen bei der Arbeit mit Inhalt helfen. In diesem Zusammenhang finden oft auch die Begriffe Frontend und Backend Verwendung. Das Frontend, die «Frontseite» des CMS, ist das, was die Lesenden angezeigt bekommen – zum Beispiel das Layout einer Webseite.

```{figure-md} wordpressfrontend
<img src="./assets/wordpress.frontend.png" alt="WordPress Frontend" class="">

(WordPress-)Frontend der Seite www.hfmt.ch/sfgz (eigene Abbildung)
```

Das Backend, die «Rückseite», ist die Arbeitsoberfläche für Autoren. Es sieht in der Regel ganz anders aus als das Frontend. Im Backend können die Autoren ihre Inhalte erfassen, Bilder bearbeiten, oder Artikel aufschalten und wieder offline nehmen (verbergen).

```{figure-md} wordpressbackend
<img src="./assets/wordpress.backend.png" alt="WordPress Backend" class="">

(WordPress-)Backend der Seite www.hfmt.ch/sfgz (eigene Abbildung)
```

Ein CMS hat wesentliche Merkmale (nach {cite:ps}`bohringer_kompendium_2008`, S. 737):

* Mehrere Autoren können gleichzeitig am Content arbeiten und brauchen keine Programmierkenntnisse für dessen Verwaltung.
* Der Content kann jederzeit aktualisiert werden.
* Es gibt eine strikte Trennung zwischen Backend und Frontend. Das erleichtert insbesondere die Administration.
* Backend und Frontend können im Browser aufgerufen werden und sind daher unabhängig vom jeweiligen Betriebssystem, wo der Browser läuft.
* Die Inhalte können bequem mit einer Suchfunktion durchsucht werden.
* Ein CMS kann einfach erweitert werden. Alle grossen CMS am Markt bieten Schnittstellen und eine Plugin-Infrastruktur.
* Die Inhalte können bei Bedarf zeitlich gesteuert werden.
* Es lassen sich ganze Redaktions-Workflows abbilden, die es erlauben, Autoren verschiedene Rechte auf die Freigabe und die Sperrung von Inhalten zu geben.
* ...

### Bestandteile

Ein CMS, MAM oder DAM besteht in der Regel aus mehreren technischen Komponenten. Wir erklären sie dir hier am Beispiel des CMS.

Ein CMS besteht in der Regel aus einer Programmierung und aus einer Datenbank. Die Programmierung ist das eigentliche Programm und die Datenbank für die Ablage der Inhalte. Es gibt also sozusagen eine Trennung zwischen Logik und Inhalt. Die Programmierung und die Datenbank wird meist auf einem oder mehreren Servern betrieben. Beliebte Programmiersprachen für den serverseitigen Code eines CMS sind zum Beispiel:

- **Java** (siehe {cite:ps}`kersken_it-handbuch_2019`, S. 504)
- **JavaScript/Node.js** (siehe _«Was ist JavaScript?»_ von {cite:ps}`mdn_contributors_was_2021`)
- **PHP** (siehe _«Was ist PHP?»_ von {cite:ps}`the_php_group_was_nodate`)
- **Python** (siehe {cite:ps}`kersken_it-handbuch_2019`, S. 530)
- **Ruby** (siehe _«Ruby-Programmierung: Einleitung»_ von {cite:ps}`wikibooks_contributors_ruby-programmierung_2018`)

Die grösste Verbreitung hat wahrscheinlich die Programmiersprache PHP. So sind zum Beispiel die CMS-Plattformen von Drupal, WordPress und Joombla damit programmiert.

### Technischer Ablauf

Im klassischen Workflow fragt der Browser beim Webserver eine bestimmte Seite der Webseite an – zum Beispiel [www.hfmt.ch`/sfgz`][sfgz]. Der Webserver nimmt diese Anfrage entgegen und leitet sie and das CMS weiter – in diesem Beispiel WordPress. Die PHP-Programmierung des WordPress nimmt die Anfrage entgegen und fragt den Inhalt der Seite [www.hfmt.ch`/sfgz`][sfgz] bei der Datenbank an. Die Datenbank liefert den Inhalt der Seite [www.hfmt.ch`/sfgz`][sfgz] an die PHP-Programmierung von WordPress zurück. Danach setzt WordPress die Daten aus der Datenbank in die angeforderte Seite ein. Dieser Vorgang wird gerne auch als Rendering bezeichnet. Das Rendering beschreibt den Augenblick, wo WordPress die HTML-Webseite zusammenbaut, welche später im Browser angezeigt werden soll. Sobald das Rendering abgeschlossen ist, liefert WordPress die Seite zurück and den Webserver und der Webserver schickt sie zurück an den Browser, wo sie dem Benutzer angezeigt wird.


```{figure-md} cmsflow
<img src="./assets/cmsflow.png" alt="CMS Flow" class="">

CMS Deployment Diagram (UML mit Kommentaren, eigene Abbildung)
```

### Beispiele

#### Datenbank

Die Seite «SFGZ» mit der URL [www.hfmt.ch`/sfgz`][sfgz] in der Datenbank:

```{figure-md} wordpressdb
<img src="./assets/wordpress.database.png" alt="WordPress Database" class="">

Screenshot WordPress Datenbank mit phpMyAdmin Frontend (eigene Abbildung)
```

```{tip}
Mehr zum Thema Datenbanken erfährst du im Abschnitt [Datenbanken](./datenbanken.md).
```

#### Programmierung (PHP)

Ausschnitt des verantwortlichen PHP-Templates, welches mit den Daten aus der Datenbank das HTML der Seite [www.hfmt.ch`/sfgz`][sfgz] rendert:

```php
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hfmt
 */

get_header();
?>
  <main id="primary" class="site-main">
    <?php
    while ( have_posts() ) :
      the_post();
      get_template_part( 'template-parts/content', 'page' );
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
    endwhile;
    ?>
  </main>
<?php
get_sidebar();
get_footer();
```

#### HTML im Browser

Das fertige HTML im Browser:

```{figure-md} wordpresshtml
<img src="./assets/wordpress.html.png" alt="WordPress HTML" class="">

Screenshot WordPress HTML im Browser (Ausschnitt, eigene Abbildung)
```

## Informationsarchitektur

Die Inhalte einer Webseite werden gerne strukturiert und in einer Hierarchie geordnet. Die meisten CMS erlauben es, Inhalte horizontal, vertikal oder chaotisch zu verknüpfen.

### «Vertikal»

Die vertikale Verknüpfung kannst du dir wie eine Art Organigramm vorstellen, das vertikal von oben nach unten verläuft. Zu oberst steht in der Regel die Hauptseite deiner Webseite, die gerne auch als «Home» bezeichnet wird. Bei der Webseite der HF Medientechnik ist das die Seite, die erscheint, wenn du [www.hfmt.ch][home] aufrufst. Sie gibt dir eine inhaltliche Übersicht zur Schule und führt dich in den Inhalt der Webseite ein. Von da aus gelangst du auf verschiedene Unterseiten, die das eine oder andere Thema weiter vertiefen – zum Beispiel die angebotenen Fächer unter [www.hfmt.ch/faecher][faecher].

```{figure-md} cmsinfarchitecture
<img src="./assets/cmsinfarchitecture.png" alt="Vertikale Architektur" class="">

Ausschnitt der «vertikalen» Hierarchie von [www.hfmt.ch][home] (simplifiziertes UML, eigene Abbildung)
```

### «Horizontal»

Die horizontale Verknüpfung von Inhalten funktioniert etwas anders. Sie ordnet Inhalte nicht hierarchisch von oben nach unten, sondern eher entlang eines Themas. Als Beispiel kannst du dir den HFMT-Blog unter [www.hfmt.ch/blog][blog] anschauen. Die Blog-Seite selbst ist noch Teil der vertikalen Struktur. Sie fasst die Anrisstexte aller Blog-Posts zusammen und ordnet sie auf mehreren Seiten der gleichen Informationsebene. Wenn du aber bei einem Blogbeitrag auf weiterlesen klickst, kommst du in eine horizontale Struktur. Das erkennst du vielleicht ein bisschen ander URL aber ganz sicher am Ende des betreffenden Beitrags. Da sind nämlich eine Reihe von Kategorien und Tags aufgelistet.

```{figure-md} infarchtags
<img src="./assets/infarch.tags.png" alt="Screenshot von Kategorien und Tags am Ende eines Blog-Posts" class="">

Screenshot von Kategorien und Tags am Ende eines Blog-Posts auf [www.hfmt.ch][home] (eigene Abbildung)
```

Diese Kategorien und Tags bieten dir die Möglichkeit, dich horizontal durch die Themen des Blogs zu bewegen. Sie ordnen den Inhalt des aktuellen Blog-Beitrags nach verschiedenen inhaltlichen Gesichtspunkten ein. Wenn du zum Beispiel auf eine Kategorie klickst, kommst du auf eine Übersichtsseite, die dir alle Beiträge der betreffenden Kategorie anzeigt.

```{figure-md} infarchcategory
<img src="./assets/infarch.category.png" alt="Screenshot Kategorie-Seite für «HFMT19» auf [www.hfmt.ch][home]" class="">

Screenshot Kategorie-Seite für «HFMT19» auf [www.hfmt.ch][home] (Ausschnitt, eigene Abbildung)
```

Wenn du dagegen auf den Link eines Tags klickst, kommst du auf eine Übersicht aller Beiträge, die das betreffende Tag enthalten – unabhängig von der Kategorie der Beiträge. Im Fall von [www.hfmt.ch][home] sind also Kategorien und Tags Strukturierungshilfen, die dir erlauben, dich in Themenfeldern zu bewegen. Für das CMS hinter [www.hfmt.ch][home] sind Kategorien und Tags aber nicht mehr als Zeichen und Nummern, die durch die Redaktion verknüpft wurden.

```{figure-md} cmsinfarchitecture2
<img src="./assets/cmsinfarchitecture2.png" alt="Horizontale Architektur" class="">

Ausschnitt der «horizontalen» Hierarchie von [www.hfmt.ch][home] (simplifiziertes UML, eigene Abbildung)
```

## Jamstack

Die Entwicklung von CMS hat eine lange Historie. Viele der etablierten CMS sind schon 20 oder mehr Jahre alt. Sie lösten um die Jahrtausendwende und in den frühen 2000er-Jahren unter anderem statische HTML-Seiten ab. Bei denen wurde das HTML für den Benutzer nicht durch das CMS berechnet, sondern vorab von Hand durch die Redaktion einer Seite codiert. Es gab zwar erste Produkte, mit denen sich HTML-Inhalte auch visuell bearbeiten liessen. Das waren aber meist Programme, die auf dem Computer installiert wurden. Dagegen hatte das CMS den Vorteil, dass die Verwaltung der Inhalte eben direkt und bequem online, also auf der Seite selbst, gemacht werden konnte.

Über die Jahre sind die gängigen CMS mit den Anforderungen, die an sie gestellt wurden, gewachsen. Heute sind sie in der Regel ein Geflecht aus ganz vielen grossen und kleinen Komponenten. Diese Vielfältigkeit kann verschiedene Nachteile für die Geschwindigkeit oder die Wartung haben. Aber warum ist das so? Wir erinnern uns: ein CMS besteht meist aus einer Programmierung, einer Datenbank und einer HTML-Ausgabe, die dem Webserver übergeben wird. Von diesem Konzept sind die gängigen CMS bis jetzt nie wirklich abgewichen. Im Unterschied zu den frühen 2000er-Jahren wären heute aber auch andere technische Konzepte denkbar. Wenn ich also nun gleich den Begriff des Jamstack einführe, dann ändert sich für die Benutzer und Redaktoren einer Webseite wahrscheinlich nicht viel. Es geht beim Jamstack ({cite:ps}`netlify_for_nodate`) vielmehr darum, die technische Basis für Content Management zu verändern. Um das zu erreichen, versuchen die Entwickler von Jamstacks im wesentlichen drei Sachen anders zu machen:

* Sie Verzichten wenn möglich auf eine eigene Datenbank.
* Sie versuchen, das HTML der Webseite wird nicht auf dem Server, sondern auf dem Client zu rechnen.
* Sie setzen Microservices ein.

```{figure-md} jamstackarchitecture
<img src="./assets/jamstack.architecture.svg" alt="Jamstack Architektur von Netlify" class="">

Jamstack Architektur (Netlify, [jamstack.org][jamstack])
```

### Der Verzicht auf eine Datenbank

Viele Seiten brauchen eigentlich keine Datenbank – zum Beispiel weil sie keine dynamischen Inhalte haben, die sich ständig ändern. Ein gutes Beispiel ist diese Dokumentation. Sie besteht einfach aus ein paar HTML-Seiten. Natürlich hätten wir sie auch mit einem CMS wie WordPress oder Drupal machen können. Der Wartungsaufwand wäre aber viel höher, als wenn wir «nur» ein paar HTML-Seiten ausgeben. Konkret werden die Inhalte im Markdown-Format ({cite:ps}`gruber_daring_2004`) erfasst und dann mit Hilfe von Sphinx ({cite:ps}`brandl_overview_nodate`) in HTML-Seiten umgerechnet. 

```{figure-md} jamstackmd
<img src="./assets/jamstack.md.png" alt="Markdown Text" class="">

Markdown-Text dieser Seite (eigene Abbildung)
```

Sphinx ist ein sogenannter Static Site Builder. Es ist also ein Programm, das die Markdown-Inhalte auf Kommando in HTML-Seiten umwandelt und an einem bestimmten Ort ablegt. Von dort kann sich der Webserver direkt das vorberechnete HTML holen und dir anzeigen. Die Inhalte müssen nicht zuerst aus einer Datenbank abgefragt und von PHP in eine HTML-Seite eingebettet werden.

```{figure-md} jamstackhtml
<img src="./assets/jamstack.html.png" alt="HTML Text" class="">

Mit Sphinx generiertes HTML dieser Seite (eigene Abbildung)
```

### HTML auf dem Client rechnen

Bei diesem Verfahren wird das HTML nicht von PHP auf dem Server, sondern meist durch Javascript direkt in deinem Browser berechnet. Früher waren Computer nicht so schnell, heute sind sie aber leistungsstarke Rechenmaschinen. Das heisst, ein durchschnittlicher Computer hat in der Regel mehr Systemressourcen zur Verfügung, als ein Server pro User zur Verfügung hat. Darum ist es auch nicht verwunderlich, wenn das berechnen der HTML-Seite für den Browser auf deinem Computer schneller geht als auf dem Server. Ein Nebeneffekt ist zudem, dass HTML relativ viel Netzwerkbandbreite braucht, auch wenn es nur wenig Daten transportiert. Wenn nun ein Entwickler nur noch die reinen Daten und nicht das gesamte HTML über das Netzwerk schicken muss, hat er gerade einen doppelten Vorteil: 1. seine Inhalte sind schneller übermittelt und 2. seine Inhalte sind schneller in eine HTML-Seite eingerechnet, die er ausgeben kann. Für dich als Benutzer ist dann die Seite viel schneller geladen, als wenn sie jedes Mal aus der Datenbank über PHP und das Netzwerk kommt.

### Microservices & Schnittstellen

Microservices ({cite:ps}`red_hat_inc_was_nodate`) kannst du dir, als die granulare Zerlegung von Funktionalitäten vorstellen. Bei CMS hängen alle Funktionen immer irgendwie zusammen. Das heisst, wenn du zum Beispiel auf deiner WordPress-Webseite eine Suche hast, dann funktioniert diese Suche nur zusammen mit WordPress, weil sie integraler Bestandteil des CMS ist. Bei Microservices versuchen die Entwickler jede Funktion für sich einzeln du denken und diese einzeln über eine Schnittstelle (API) einsetzbar zu machen. Ein Beispiel dafür ist die Suche von algolia ({cite:ps}`algolia_inc_site_nodate`). Sie indexiert alle HTML-Seiten einer Webseite und verlinkt die Ergebnisse dann auf diese.

### CMS oder Jamstack

Das lässt sich leider nicht so einfach beantworten und kommt ganz auf den Anwendungszweck und die Informationsarchitektur an. Für eine einfache Seite mit wenig Änderungen, so wie zum Beispiel diese Dokumentation, kann ein Jamstack oder Teile davon ganz interessant sein. Wenn wir diese Dokumentation mit einem CMS gemacht hätten, müssten wir dieses regelmässig aktualisieren – zum Beispiel um Sicherheitslücken von PHP zu schliessen. Dagegen haben wir mit statisch generiertem HTML fast keinen Wartungsaufwand, weil es final ist. Es gibt viel weniger Angriffspunkte, wo Hacker einfallen können.

## Webserver & Webhosting

Ein Webserver brauchst du, damit du eine Webseite ins Internet stellen kannst. Du kannst diesen selber betreiben oder als Dienstleistung einkaufen. Wie das genau geht – und was es für Produkte und Dienstleistungskategorien gibt, erklären wir dir unter [Services](./services.md).



[blog]:https://www.hfmt.ch/blog/
[faecher]:https://www.hfmt.ch/faecher/
[home]:https://www.hfmt.ch/
[jamstack]:https://jamstack.org/
[sfgz]:https://www.hfmt.ch/sfgz/
