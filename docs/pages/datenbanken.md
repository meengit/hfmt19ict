# Datenbanken

Mit Datenbanken können grosse Datenmengen gespeichert werden. Eine Datenbank besteht in der Regel aus zwei Elementen:

1. *Database Management System (DBMS)*
2. Daten oder Datensammlung

![Datenbank](./assets/database.png)

Das DBMS ist ein Anwendungsprogramm. Mit ihm können die Daten einer Datenbank verwaltet werden. Das heisst, es kann zum Schreiben, Lesen, Suchen, Sortieren oder formatieren von Ausgaben dieser Daten verwendet werden.

In Bezug auf die Daten in Datenbanken können folgende Definitionen helfen (nach Kersken, S. 763 & S. 764):

* *Stammdaten*: Meist unveränderliche oder selten veränderte Informationen, die dauerhaft Auskunft über Objekte und Sachverhalte geben, z. B. Bestellnummern;
* *Bewegungsdaten*: Daten die ständig in Bewegung sind, z. B. Bestellungen oder Saldo;
* *Rechendaten*: Daten die als Berechnungsgrundlage dienen, z. B. Preise;
* *Ordnungsdaten*: Daten für die Klassifizierung oder Filterung von Informationen, z. B. Postleitzahlen.

## Datenbanktypen

Es gibt viele unterschiedliche Datenbanktypen. In der Medientechnik sind unter anderem gebräuchlich:

| Typ | Erklärung | Beispiele
|-----|-----------|----------- 
| *Relationale Datenbanken* | <p style="line-height: 1.5rem;">*Relationale Datenbanken* bestehen aus <br />einer oder mehreren Tabellen, <br />die relational verknüpft sein können. | <p style="line-height: 1.5rem;">Microsoft SQL (2020), <br />MySQL (2020), <br />MariaDB (2020), <br />Oracle (2020), <br />IBM DB2 (2020)</p>
| *XML-Datenbanken* | <p style="line-height: 1.5rem;">Bei *XML-Datenbanken* werden die <br />Informationen in Form von <br />*XML-Dokumenten* abgespeichert.</p> | <p style="line-height: 1.5rem;">BaseX (2020), <br />Berkeley DB XML (2020)</p>
| <p style="line-height: 1.5rem;">*Bilddatenbanken* oder *DAM*<br />(*Digital-Asset-Management*)</p> | <p style="line-height: 1.5rem;">*DAM* sind in der Regel erweiterte, <br />relationale Datenbanksysteme, welche <br />die Verwaltung von Bildern, Audio <br />oder Videos erlauben. So können <br />zum Beispiel Medien kategorisiert <br />und durchsucht werden, wobei den <br />Metadaten eine besondere Wichtigkeit <br />zukommt. Die Grenzen zwischen *DAM* <br />und *Media-Asset-Management*-Systemen <br />*MAM* sind fliessend. *MAM* <br />sind meist auch *DAM*, jedoch <br />mit erweiterten Funktionen wie <br />zum Beispiel dem Transcoding <br />für Videodaten.</p> | <p style="line-height: 1.5rem;">Canto Cumulus (2020), <br />censhare (2020)</p>
| *NoSQL-Datenbanken* | <p style="line-height: 1.5rem;">*NoSQL* ist ein Sammelbegriff für neuere <br />dokumentenbasierte Datenbanksysteme. <br />Streng genommen sind auch <br />*XML-Datenbanken* *NoSQL* <br />Datenbanken. Da sie aber <br />teilweise über SQL abgefragt <br />werden können, zählen wir <br />sie hier nicht zu dieser <br />Kategorie.</p> | <p style="line-height: 1.5rem;">MongoDB, Format: <br />*Binary JSON, <br />BSON* (2020), <br />Redis, Format: <br />*In-Memory*, <br />*Key-Value-Store* (2020)</p>

