# Responsive Webdesign

*Responsive Web Design* steht für reaktionsfähiges oder reagierendes Webdesign. Der Begriff wurde erstmals 2010 vom Amerikaner Ethan Marcotte aufgegriffen (2020).

*Responsive Webdesign* beschreibt ein Paradigma in der Erstellung und Gestaltung von Webseiten. Es ist darauf ausgerichtet, dass das Design am jeweiligen Endgerät respektive am Format und den Fähigkeiten seiner Anzeige ausgerichtet wird. Am Bildschirm eines Desktop Computers soll das Design geräumig sein und auf die Steuerung mit der Maus – zum Beispiel einen Klick – reagieren. Auf mobilen Geräten wie Smart- und Touchphones soll es dagegen platzsparend sein und auf Touchbefehle und Gesten wie Wischen reagieren. In der Umsetzung ist die technische Basis meist HTML5, CSS3 und Javascript – wobei den *Mediaqueries* (MDN, «Using media queries», 2020) in CSS besondere Bedeutung zukommt.

Nach Ethan hat Responsive Design drei Zentrale Elemente (Zillgens, 2013, S. 15):

> 1. Ein flexibles Gestaltungsraster
> 2. Flexible Bilder und Medien
> 3. Mediaqueries, ein Modul aus der CSS3-Spezifikation

![Responsive Webdesign Beispiel](./assets/responsive-grid.png)

<small><em>Beispiel eines Rasters über drei Gerätekategorien hinweg.</em></small>

Der Gestaltungsraster wird meist als «Grid» bezeichnet. Er besteht aus Spalten (Columns), Gutter (Zwischenräume) und Aussenabständen (Margins), entlang welcher die Elemente der Gestaltung ausgerichtet werden. Die Spalten selbst respektive deren Anzahl stehen meist in Bezug zu einer Gerätekategorie. Eine gute Einführung zu dieser Aufstellung bietet das [Material Design Projekt](https://material.io/design/layout/responsive-layout-grid.html#columns-gutters-margins) von Google («Responsive layout grid », 2020).
