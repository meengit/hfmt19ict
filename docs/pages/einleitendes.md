# Einleitendes

## Informationstechnologien

> Technologien zur Gewinnung, Speicherung und Verarbeitung von Informationen *(Dudenverlag, «Informationstechnologien», 2019)*

Informationstechnologien: Das ist ein langes und umständliches Wort. Es kann vielfältig ausgelegt werden. Im Unterricht folgen wir der oben zitierten Auslegung aus dem Duden. Wir fokussieren darin auf die grundlegende Infrastruktur. Damit sind jene Komponenten gemeint, die Ressourcen für weitere Systeme zur Verfügung stellen. Es geht also um den Aufbau von Servern und Diensten – zum Beispiel für den Austausch von Daten oder das verschicken von Nachrichten. Weiterführend werde ich im weiteren Sprachgebrauch Informationstechnologien konsequent mit ICT abkürzen.

## Quellen

Die Dokumentation lehnt sich an das *IT-Handbuch für Fachinformatiker* aus dem Rheinwerk Verlag an. Im Projektmanagement werden zudem Teile aus *ICT-Projektplanung und Überwachung* und *Application Engineering* von *Compendio* angewendet. Alle Informationen zu den referenzierten Quellen sind im Abschnitt [Quellen](./quellen.md) ersichtlich.
