# Hardware Grundlagen

In der Alltagssprache werden die physischen Komponenten des Computers gerne und dem Begriff *Hardware* zusammengefasst. In diesem Skript lehne ich mich an diesen Terminus an. *Hardware* meint im Folgenden die Summe aller physischen Komponenten in Bezug auf ihren Kontext.

## Zentraleinheit

Die Hardware kann – ganz grundsätzlich – in zwei Gerätegruppen unterteilt werden. Die erste Gruppe beschreibt die Geräte der *Zentraleinheit*. Ohne diese Geräte kann ein Computer nicht funktionieren. Es sind dies im Wesentlichen:

* Prozessor (CPU)
* Arbeitsspeicher (RAM)
* Busse und Anschlüsse (Mainboard)
* BIOS oder EFI/UEFI.

## Peripherie

Die zweite Gruppe beschreibt alle peripheren Teile. Das sind in der Regel Geräte, die an die Zentraleinheit angeschlossen werden. Das sind zum Beispiel Ein- und Ausgabegeräte wie Drucker und Tastatur.

![hardware uebersicht](./assets/hardware-uebersicht.png)

*(Aus Kersken, S. 120)*

## Die CPU

![cpu](./assets/cpu.jpg "cpu")

Die *Central Processing Unit*, kurz CPU, ist das Herz des Computers. Sie übernimmt die Steuerung und führt Programme aus. Die CPU kann aus einem oder mehreren Kernen bestehen. Weitl√§ufig bekannt sind in diesem Kontext Begriffe wie *Dual-* oder *Quad-Core*. Sie stehen für eine CPU mit zwei respektive vier Kernen. Eine CPU mit vier Kernen heisst jedoch nicht automatisch, dass mehr Leistung verfügbar ist. Sie kann ihre Leistung nur dann entfalten, wenn das verwendete Betriebssystem diese ansprechen kann.

Die CPU wird in der Regel auf dem Mainboard aufgesteckt. Nicht jede CPU passt auf jedes Mainboard. Es gibt verschiedene Bauformen und Sockel (=Steckpl√§tze).

### CPU Cache

Viele CPU haben neben der Recheneinheit einen Zwischenspeicher – auch *Cache* genannt. Der Prozesser legt in diesem Cache Daten ab, die bald wieder benötigt werden. Moderne Prozessoren haben meist 2-3 Caching Levels. Dieser Cache darf nicht mit dem Arbeitsspeicher (RAM) verwechselt werden. Er ist wesentlich schneller und wesentlich kleiner und teurer.

#### L-Cache Typen

##### L1-Cache

* **Grösse:** 4 bis 256 KBit/Kern, Getrennt für Befehle und Daten
* **Aufgabe:** Enthält am häufigsten benötigte Befehle und Daten, Getrennt für Befehle und Daten
* **Eigenschaften:** Klein und sehr schnell

##### L2-Cache

* **Grösse:** 64 bis 512 KBit/Kern, mehrere MB wenn kein L3-Cache vorhanden
* **Aufgabe:** Puffer für Arbeitsspeicher, beschleunigt die gleichzeitige Ausführung mehrerer Programme
* **Eigenschaften:** Grösser, aber langsamer als L1-Cache

##### L3-Cache

* **Grösse:** 2 bis 32 MB gemeinsam für alle Kerne, nur in höherwertigen Prozessoren vorhanden
* **Aufgabe:** Abgleich der Caches aller Kerne, Datenkonsistenz und Datenaustausch zwischen Kernen
* **Eigenschaften:** Von allen Kernen gemeinsam genutzt

*(Cyberport, «Cache (CPU)», 2019)*

### Leistungsmerkmale einer CPU

Das wichtigste Leistungsmerkmal einer CPU ist ihre *Wortbreite*. Sie beschreibt, aus wie vielen Bits das Maschinenwort der CPU besteht. Je breiter ein Maschinenwort, desto mehr kann sie im gleichen Durchgang bearbeiten. Indes können die einzelnen Komponenten einer CPU unterschiedliche Wortbreiten aufweisen.

| Jahr | Wortbreite | Anzahl Zust√§nde |
| ---- | ---------- | --------------- |
| 1971 | 4 Bit      | 16              |
| 1974 | 8 Bit      | 256             |
| 1978 | 16 Bit     | 65536           |
| 1985 | 32 Bit     | > 2 Milliarden  |
| 1992 | 64 Bit     | > 18 Trillionen |

> Auszug aus: Kersken, Sascha. *IT-Handbuch für Fachinformatiker: der Ausbildungsbegleiter*. Bonn: Galileo Press, 2010, 117.

Neben der Wortbreite gibt es weitere Leistungsmerkmale, welche die Geschwindigkeit einer CPU benennen. Besser bekannt als die Wortbreite ist zum Beispiel die *Taktfrequenz*. Sie wird jedoch nicht von der CPU bestimmt, sondern ist ein Vielfaches des Mainboard-Taktes. Der Multiplikator wird mit einer Steckbrücke auf dem Mainboard eingestellt. Ist das Mainboard beispielsweise mit 133 MHz getaktet und der Multiplikator auf das Zwanzigfache eingestellt, ergibt das eine CPU-Taktrate von 2,66 GHz. Der Hersteller der CPU definiert den Maximalwert. Dieser sollte mit dem Multiplikator nicht überschritten werden.

## Arbeitsspeicher (RAM)

![ram](./assets/ram.jpg "ram")

Die Abkürzung *RAM* steht für *Random Access Memory*. Übersetzt heisst das «Speicher mit wahlfreiem Zugriff» (Kersken, 2019, S. 135). Im RAM liegen die aktuell ausgeführten Programme und ihre Daten. Random Access hat dabei zwei Bedeutungen:

* Alle Inhalte des Speichers können gelesen und verändert werden. Das Gegenteil des RAM ist das ROM – welches wir später behandeln.
* Es gibt verschiedene Verfahren für den Zugriff auf Daten. Bei Magnetbändern erfolgt er zum Beispiel sequenziell. Das heisst, der Zugriff ist gekoppelt an die Laufrichtung. Das RAM ist das genaue Gegenteil. Auf jedes Byte des Speichers kann einzeln und in beliebiger Reihenfolge zugegriffen werden.

Alle RAM-Bausteine haben gemeinsam, dass sie ohne Strom nicht funktionieren. Ihr Inhalt ist flüchtig. Bei einem Stromunterbruch gehen alle Daten verloren. Allerdings gibt es zwei verschiedene Bauformen:

* Das *Dynamic RAM (DRAM)* ist günstig in der Herstellung und braucht vergleichsweise wenig Strom. Jedoch benötigt es nicht nur das Anliegen einer Spannung. Der Inhalt jeder Speicherstelle muss mit jedem Taktzyklus aufgefrischt werden.
* Das *Static RAM (SRAM)* braucht mehr Strom als das DRAM. Dafür braucht es nur eine anliegende Spannung. Die Speicherstellen müssen nicht mit jedem Zyklus aufgefrischt werden. SRAM-Bausteine arbeiten wesentlich schneller als DRAM, sind jedoch auch viel teuerer in der Herstellung. Daher werden sie meist nur für die Caches und nicht für die Realisation des gesamten Arbeitsspeichers eingesetzt.

Das RAM ist immer in einzelnen Speicherzellen organisiert. Jede Speicherzelle ist 1 Byte gross und hat eine eigene Adresse. Wie sie im angesprochen werden, ist abhängig vom Prozessor und indirekt vom Betriebssystem.

Der Arbeitsspeicher hat seinen Platz in der Regel auf dem Mainboard. Die Chips werden senkrecht aufgesteckt. Die aktuellen DIMM-Modulen* werden vorsichtig in den RAM-Slot gedrückt, bis die beiden Seitenhebel merklich einrasten.

Die gängigen Bauarten für RAM-Bausteine sind zur Zeit:

* Das *Double Data Rate (DDR) RAM* besteht aus DIMM-Modulen, die 168 Kontakte haben. Sie können mit der doppelten Datenrate gegenüber den herkömmlichen SD-RAMs arbeiten. Inzwischen ist die vierte Generation DDR4 dieses Typs auf dem Markt.
* Praktisch ausgestorben ist das Nicht-DDR-SD-RAM. Es wurde ebenfalls in Form von DIMM-Modulen geliefert. Der Zugriff auf den Speicher erfolgte jedoch in der Taktfrequenz des Mainboards.

Eine Alternative zu diesen beiden Bauarten ist das *Rambus-RAM (RD-RAM)*. Es ist etwas schneller als das SD-RAM und das ursprüngliche DDR-RAM. Es hat jedoch einige Besonderheiten. So kann es nur mit speziellen Mainboards betrieben werden und wird nur von der Firma Rimbus hergestellt.

Für den flüssigen Betrieb eines Bürocomputers sind 4-8 GB RAM nötig. In der Bildbearbeitung und in der Entwicklung sollten es mindestens 16 GB sein. Bei Serversystem ist der Bedarf oft viel höher. Die Werte verändern sich jedoch über die Zeit. Die Angaben in diesem Abschnitt beziehen sich auf das Jahr 2019. Sie werden schon bald überholt sein.

`*` *DIMM* steht für *Double Inline Memory Modules*

## ROM

![rom](./assets/rom.jpg "rom")

Das *Read-only Memory (ROM)* kann von Programmen nur gelesen werden. Im Unterschied zum RAM kann es dafür seine Daten auch dann speichern, wenn kein Strom vorhanden ist.

Es gibt heute verschiedene Bauarten für ROM. Das ursprüngliche ROM* hat eine fest verdrahtete Funktionalität. Es kann nicht verändert werden.

* Das *Programmable ROM (PROM)* kann ein einziges Mal programmiert werden. Die Bits des Speichers werden bei diesem ersten und einzigen Programmiervorgang durch hohe Spannungshitze auf die Platine «gedampft».
* Das *Erasable ROM (EPROM)* kann mit einem EPROM-Brenner geschrieben und wieder gelöscht werden. Für den Vorgang ist also ein spezielles Gerät nötig.
* Im Unterschied zum EPROM kann das *Electronically Erasable PROM (EEPROM)* den Speicher elektronisch löschen und neu beschreiben. Danach bleiben die Daten auch ohne Strom erhalten.
* Das *Flash-EEPROM* oder *Flash-EPROM* ist die jüngste Entwicklung. Sein Speicher kann wie beim EEPROM mit Software verändert werden. Es ist jedoch wesentlich günstiger in der Herstellung und braucht weniger Strom. Dafür ist der Zugriff nicht ganz so schnell wie beim EEPROM. Das *Flash-EEPROM* wird heute gerne bei BIOS-Bausteinen, Speicherkarten, USB-Sticks und SSD (Solid State Disks) Festplatten eingesetzt.

In den 1980er Jahre gab es viele Homecomputer, die ihr Betriebssystem fest im ROM hatten. Sie sind jedoch mit den heutigen Betriebssystemen kaum vergleichbar. Ihre Implementierung war, verglichen mit heute, sehr rudimentär. IBM gab dieses Verfahren als erster Hersteller auf. Die IBM-PC wurden schon sehr früh auf einen Datenträgen ausgelagert (5¼ Zoll-Disketten).

Heute gibt es noch eine Reihe von Spezialcomputern, die das Betriebssystem fest im ROM haben. Das sind zum Beispiel im Netzwerkbereich Router- oder Firewall-Boxen.

## Basic Input Output System BIOS

![BIOS](./assets/bios.jpg "BIOS")

Das *BIOS* enthält in der Regel ein Pogramm in der Maschinensprache es Computers. Es wird beim Einschalten des Computers an einen bestimmten Ort im Arbeitsspeicher geladen. Der Prozessor weiss, dass er zu Beginn genau dieses Programm ausführen muss.

Das Programm testet als Erstes, ob die Hardware des Computers funktioniert. Es schaut zuerst nach der Grafikkarte und danach nach dem RAM. Anschliessend prüft es, ob ein Laufwerk mit einem Betriebssystem vorhanden ist. Dieser Vorgang wird auch als *Power-on Self Test (POST)* bezeichnet. Falls Probleme auftreten, werden Tonsignale in bestimmten Abfolgen ausgegeben. Die se Abfolgen können mit dem Manual des Herstellers aufgeschlüsselt werden.

Neben dem POST hat das BIOS auch Funktionen zur Kommunikation mit der Hardware. Diese Funktionen können von Betriebssystemen oder Programmen bei Bedarf genutzt werden. In der Regel werden sie jedoch umgangen. Stattdessen sprechen moderne Betriebssysteme die Treiber der Hardware gerne direkt an.

Nach einem erfolgreichen POST übergibt das BIOS die Kontrolle an den Datenträger mit dem Betriebssystem. Im Startsektor, dem *Master Boot Record*, werden nun die nächsten Schritte initialisiert. Meist heisst das, dass ein *Bootloader* geladen wird, der ein Betriebssystem startet. Es kann aber auch sein, dass der Anwender über einen *Bootmanager* zwischen mehreren Betriebssystemen auswählen kann.

## Firmware

Die Firmware ist in einem elektronischen Gerät eingebettet – so zu sagen in Hardware gegossene Software. Inzwischen wird das BIOS bei Intel-Rechnern immer häufiger durch EFI (Enhanced Firmware Interface) oder die standardisierte Variante UEFI (Unified EFI) ersetzt (Kersken, 2019, S. 135).

Die Firmware sitzt zumeist in einem Flash-Speicher und ist durch den Anwender nicht oder nur mit speziellen Mitteln aus austauschbar. Der Begriff leitet sich aus dem Fakt ab, dass die die Firmware funktional fest mit der Hardware verbunden ist. Das heisst in der Regel, dass eine bestimmte Hardware ohne die zugehörige Firmware nicht benutzt werden kann. Die Firmware übernimmt damit eine spezielle Position zwischen der Hardware und der Anwendungssoftware.

## Grafikkarte (GPU)

![gpu](./assets/gpu.jpg "gpu")

Die Grafikkarte erzeugt die Anzeige am Monitor. Aktuell gibt es zwei wesentliche Bauarten. Die integrierte (*onboard*) Grafikkarte ist fest auf dem Mainboard verbaut. Sie hat keinen eigenen Videospeicher, sondern greift direkt auf das verfügbare RAM zu. Diese Variante braucht wenig Platz und wird deshalb gerne in Notebooks verbaut. Bei den technischen Spezifikationen wird sie gerne mit dem Zusatz IGP (*Integrated Graphics Processor*) ausgewiesen.

Das Gegenteil von integrierten Grafikkarten sind dezidierte (*dedicated*) Grafikkarten. Sie haben einen eigenen Videospeicher und einen eigenen Prozessor (*Graphics Processing Unit*). Bei den technischen Spezifikationen sind sie zum Beispiel über VRAM-Eintrag erkennbar. Dieser bezeichnet die Grösse des Videospeichers.

Bis ca. 1996 konnten Grafikkarten nur 2D-Grafik darstellen. Seither haben die meisten Grafikkarten einen 3D-Beschleuniger, der das *Echtzeit-Rendering* von 3D-Szenen unterstützt (Kersken, 2019, S. 169).

Viele Grafikkarten unterstützen heute die Ausgabe von 16:9 oder 16:10. Die Auflösungen reichen von FullHD (1920 x 1080px)  bis über 4K (3840 x 2160px) hinaus. Das ist besonders bei grossen Monitoren ab ca. 27 Zoll für eine gute Darstellung wichtig. 

Die Leistung einer Grafikkarte hängt von mehreren Faktoren ab (Kersken, 2019, S. 170):

* Die Leistung des Grafikprozessors;
* Die Menge und Art des verbauten RAM (SD-RAM, DDR-RAM, optimiertes RAM);
* Die Taktfrequenz des RAMDAC (*Random Access Memory Digital/analog Converter*);
* Der verwendete Anschluss für das Mainboard.

Wird die Grafikkarte direkt am PCI-Bus des Mainboards angesteckt, muss sie die verfügbaren BUS-Bandbreiten mit anderen Geräten teilen. Dagegen steht ihr zum Beispiel der AGP-Anschluss (*Accelerated Graphics Port*) exklusiv zur Verfügung. Der beste Anschluss für Grafikkarten ist heutzutage jedoch PCI Express (Kersken, 2019, S. 170).

## Massenspeicher

![rom](./assets/rom.jpg "rom")

Es gibt eine Vielzahl von Massenspeichern. Sie werden gerne in Gruppen nach Schreib- und Leseverfahren unterteilt.

Bei *magnetischen Datenträgern* werden die Daten auf eine magnetisierbare Oberfläche geschrieben. Das Verfahren wird heute noch bei Festplatten und Bandlaufwerken für Backups angewendet. Disketten-, ZIP- und Jaz-Laufwerke basierten auch auf diesem Verfahren. Sie gelten heute jedoch als veraltet und werden kaum noch eingesetzt.

*Optischen Datenträger* schreiben die Daten auf eine Metallfläche. Sie erzeugen dabei das so genannte *Pit-Muster*. Es wird durch Vertiefungen (*Pits*) und unveränderte Stellen (*Land*) gebildet. Für das Lesen werden Laser eingesetzt. Sie geben einen Lichtstrahl ab und messen dann die Reflexion der Metallfläche. Weil das *Pit* vertieft ist, reflektiert es anders als das unveränderte *Land*. So können die Lesegeräte die Datenstruktur ablesen. Die wichtigsten Beispiele dieser Kategorie sind CDs und DVDs. 

*Magneto-optische Datenträger* werden heute kaum noch verwendet. Sie bestehen aus einem Mischverfahren. Ihre Oberfläche wird durch starke Hitze magnetisch veränder- und damit beschreibbar. Anschliessend können ihre Daten mit einem Laser gelesen werden. Ein wichtiger Hersteller im Bereich dieser Datenträger war *Sy-Quest*. Seine Laufwerke waren vor allem bei Apple Mac User lange Zeit verbreitet.

### Festplatten

Der wahrscheinlich wichtigste Massenspeicher in einem Computer ist (heute) die Festplatte. Sie werden im gängigen Sprachgebrauch gerne in zwei Kategorien unterschieden. *Hard Disk Drive HDD* meint in der Regel die herkömmliche Bauweise mit rotierenden Platten. Dagegen ist *Solid State Drive SSD* eine neuere Bauform. Bei dieser werden die Daten nicht auf Platten sondern auf einen Speicher-Chip geschrieben.

#### Hard Disk Drive HDD

Eine HDD besteht aus runden, gestapelten Metallplatten, die um eine gemeinsame Achse rotieren. Zwischen den Metallplatten sind Schreib- und Leseköpfe angeordnet. Sie fahren von der Seite zwischen die Platten hinein. Die ganze Konstruktion ist vakuumverschweisst â denn die Schreib- und Leseköpfe schweben mit einer Entfernung dünner einem menschlichen Haar über den Platten. Ein kleines Sandkorn würde reichen, um diese Konstruktion zu zerstören.

Die Speicherregionen einer Festplatte werden mit dem *CHS-Verfahren* (Cylinder, Head, Sector) durchnummeriert. Dabei werden die einzelnen Platten von innen nach aussen in sogenannte *Zylinder* oder *Spuren* unterteilt und durchnummeriert.

Mit den *Kopfnummern* werden die danach die einzelnen Seiten der Platte bezeichnet. Zum Schluss gilt es noch die *Sektoren* zu erwähnen. Sie beschreiben die Einteilung des Zylinders in «Kuchenstücke».

Durch die Unterteilung in Sektoren nimmt die Datendichte nach aussen immer weiter ab, womit Speicherplatz vergeudet wird. Um diesem Effekt entgegenzuwirken wurde Das *Logical Block Addressing LBA* wirkt diesem Effekt entgegen. Es wird vor allem bei neueren Festplatten eingesetzt.

##### Partitionieren

Beim Partitionieren wird die Festplatte in logische Abschnitte unterteilt. Eine Festplatte kann eine oder mehrere Partitionen enthalten. Diese können in der Regel in drei Arten angelegt werden (Elektronik Kompendium, «Partitionen/Partitionieren», 2019).

Eine *primäre Partition* ist dann nötig, wenn ein Betriebssystem gestartet werden soll. Pro physische Festplatte sind in der Regel maximal vier *primäre Partitionen* möglich. Bei mehr als vier muss entweder der Bootsektor bearbeitet oder eine *erweiterte Partition* erstellt werden. 

Eine *logische Partition* ist nicht startfähig. Sie dient vor allem zur Ablage von Daten. Unter Windows werden logische Partitionen in der Regel mit einem Laufwerksbuchstaben zwischen C und Z angezeigt. Gut erkennbar ist das zum Beispiel, wenn ein USB-Stick an einen Windows Computer angesteckt wird.

In der Regel kann pro physisches Laufwerk eine *erweiterte Partition* erstellt werden. Sie ist nicht startfähig, kann aber weitere *logische* «Unterpartitionen» erhalten.

Einige wichtige Partitionstypen unserer Gegenwart sind:

* FAT32
* NTFS
* Apple Journaled
* Apple HFS
* Linux LVM
* FreeBSD
* RAID

#### Solid State Drive/Disk (SSD)

In den vergangenen Jahren hat sich die Bauweise der *Solid State Drive/Disk SSD* als Alternative zu den herkömmlichen HDDs etabliert. Von aussen sind sie kaum von herkömmlichen, mechanischen Festplatten zu unterscheiden. Im Innern haben sie aber keine mechanischen Teile mehr. Stattdessen speichern sie ihre Daten auf einen Flash-EPROM-Speicher ab. Sie sind dadurch meist schneller als HDDs und verbrauchen weniger Strom.


(physische-server)=
## Physische Server

In der Informationstechnik ist ein Server entweder ein Programm oder ein physisches Gerät. Dieser Abschnitt geht auf die physischen Server ein. Ein Server stellt Funktionalitäten für andere Programme oder Geräte bereit. In diesem Zusammenhang wird gerne vom Client-Server-Modell gesprochen.

Physische Server haben in der Regel eine spezielle Bauweise, reduziert auf die Zentraleinheit ohne Peripetie. Grundsätzlich kann allerdings «jeder» Computer als Server dienen, sofern darauf ein Server in Form eines Programms installiert ist. Die spezielle Bauweise von Geräten, die «nur» als Server eingesetzt werden sollen, ist jedoch darauf ausgelegt, dass die einzelnen Geräte platzsparend übereinandergestapelt werden können und in der Regel mehr Leistung haben als Desktop-Computer.

![rack](./assets/server.jpg)
