# Informationstechnologien

## Version

```text
Version: v2.1.0-alpha.3
Letzte Überarbeitung: 28. Oktober 2021
Autor: Andreas
```

Initial erstellt für die Klasse HFMT19A, erweitert für die Klasse HFMT21A, weitere Informationen zur HFMT gibts unter [www.hfmt.ch](https://www.hfmt.ch)

## Build status

[![Documentation Status](https://readthedocs.org/projects/informationstechnologien/badge/?version=latest)](https://informationstechnologien.readthedocs.io/de/latest/?badge=latest)

## Inhalt

Weiter zur [Einleitung >](./einleitendes.md)

## Lizenzierung

### Programmcode

MIT, siehe [LICENSE (in Repository)](https://gitlab.com/meengit/hfmt19ict/-/blob/master/LICENSE)

### Inhalt

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Der Inhalt ist lizenziert unter <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

## Komponenten

Wir bedanken uns ganz herzlich bei diesen Open Source Projekten:

- [Read the Docs, Inc & contributors](https://readthedocs.org/)
- [Georg Brandl and the Sphinx team](https://www.sphinx-doc.org/en/master/)
- [Executable Book Project (`MyST`)](https://myst-parser.readthedocs.io/en/latest/)
- [Matthias C. M. Troffaes (`sphinxcontrib-bibtex`)](https://sphinxcontrib-bibtex.readthedocs.io/en/latest/)

## Impressum & Datenschutz

Lesen sie dazu bitte die Angaben im [Projektverzeichnis bei GitLab](https://gitlab.com/meengit/hfmt19ict)

## Changelog

### 28. Oktober 2021, v2.1.0-alpha.3

* Abschnitt [Netzwerk & Internet](netzwerk.ch) um Feeds, RDF und SEO ergänzt

### 22. Oktober 2021, v2.1.0-alpha.2

* Abschnitt [Services](services.md) um Cloud Computing ergänzt

### 21. Oktober 2021, v2.1.0-alpha.1

* Abschnitt [CMS](cms.md) hinzugefügt
* Abschnitt [Services](services.md) initialisiert

### 6. April 2021, v2.0.0-beta

* Migration «Technik» von `mdbook` zu `sphinx` & `readthedocs.org`
* [PDF Export](https://informationstechnologien.readthedocs.io/_/downloads/de/latest/pdf/) hinzugefügt

### 19. Januar 2020, v1.1.0-beta

* Abschnitt [Datenbanken](./datenbanken.md) hinzugefügt
* Abschnitt [Design](./design.md) hinzugefügt

### 10. Januar 2020, v1.0.0-beta

* Migration [Hardware](./hardware.md) von `glitch.com` zu `gitlab.com`
* [Projektmanagment](./projektmanagement.md) hinzugefügt

