# Markdown

[Markdown][1] ist eine «[einfache][2]» [Auszeichnungssprache][2]. Sie wurde von John Gruber und Aaron Swatz im Jahr 2004 veröffentlicht.

Markdown war ursprünglich als Text zu HTML Übersetzungsprogramm für Web-Autoren gedacht. Heute kann die Auszeichnungssprache in viele weitere Formate übersetzt werden.

Markdown besteht aus zwei Teilen. Als Erstes ist es eine Syntax zur Formatierung von Text. Als Zweites ist es ein Stück Software, das Dokumente in der Markdown-Syntax in andere Formate übersetzen kann. In diesem Kontext wird oft vom Markdown-[Parser][4] oder seltener vom Markdown-[Compiler][5] gesprochen.

Das übergeordnete Ziel der Markdown-Syntax ist die Lesbarkeit. Die Syntax ist so konzipiert, dass sich auch ohne Formatierung mit Schriftgrössen oder Farben eine gute Lesbarkeit ergibt.

Markdown-Dokumente sind grundsätzlich einfache Text-Dokumente – auch [Plain Text][6] genannt. Sie können mit fast jedem Textverarbeitungsprogramm geöffnet werden. Während oder nach dem Editieren kann ein Markdown Dokument einem Parser übergeben werden. Dieser wandelt den Text in ein Ausgabeformat um – zum Beispiel in ein HTML. Das HTML kann mit CSS formatiert und ansprechend ausgegeben werden.

## Syntax

Heute gibt es Markdown in verschiedenen Dialekten. Sehr oft wird [GitHub Flavored markdown (GFM)][7] verwendet. Weitere Dialekte sind [MultiMarkdown][8] oder – etwas jünger – [MDX][9].

In dieser Dokumentation verwenden wir GitHub Flavored Markdown, künfigt mit dem Kürzel GFM bezeichnet. Einen gute Übersicht zu fast allen Möglichkeiten von GFM findet ihr bei [Mastering Markdown][7] von GitHub.

![Übersicht Markdown](https://cdn.glitch.com/04cd3964-92fa-448d-a169-c769764a9fb5%2Fmarkdown-preview.png?v=1570225892774)

Links: Markdown-Dokument geöffnet in einem Editor; Rechts: Voransicht mit Formatierung

[1]:https://daringfireball.net/projects/markdown/
[2]:https://en.wikipedia.org/wiki/Lightweight_markup_language
[3]:https://de.wikipedia.org/wiki/Auszeichnungssprache
[4]:https://de.wikipedia.org/wiki/Parser
[5]:https://de.wikipedia.org/wiki/Compiler
[6]:https://de.wikipedia.org/wiki/Plain_text
[7]:https://guides.github.com/features/mastering-markdown
[8]:https://fletcher.github.io/MultiMarkdown-6/
[9]:https://mdxjs.com/

