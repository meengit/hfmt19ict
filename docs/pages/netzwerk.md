# Netzwerk & Internet

Ein Netzwerk verbindet mehrere Computer für den Austausch von Daten. Es gibt verschiedene Arten von Netzwerken. Das eigene Netzwerk, also das Netzwerk, in dem sich ein Computer gerade befindet, wird meist als *lokales Netzwerk*, auch *Local Area Network LAN* bezeichnet. Das *WLAN* ist dementsprechend das *Wireless Local Area Network*.  Das *LAN* ist in der Regel mit einem *Wide Area Network* verbunden, um letztendlich Daten in andere *LANs* zu übertragen.

Netzwerke können in verschiedenen Topologien implementiert werden (Kersken, 2019, S. 202 & 203):

* *Bustopologie:* Netzwerkknoten (Anschlüsse) hintereinander in einem einzelnen Kabelstrang;
* *Sterntopologie:* Alle Knoten sind über ein eigenes Kabel mit einem zentralen Gerät oder zentralen Geräten verbunden;
* *Ringtopologie:* Die Netzwerkknoten (Anschlüsse) sind hintereinander an einem zentralen Kabelstrang angeordnet, der als Ring angeordnet ist. Im Unterschied zur * Bustopologie* bleibt die *Ringtopologie* geschlossen;
* *Baumtopologie:* Zusammenschluss verschiedener Netzwerksegmente.

![Topologien](./assets/topologien.png "Topologien")

Die gebräuchlichste Form ist vermutlich die *Sterntopologie* sowie freie Anwendungen, die verschiedene Topologien kombinieren.

## Internet

Das Internet als Ganzes ist ein weltweiter Verbund von Rechnernetzwerken. Es ermöglicht im Wesentlichen die Nutzung von Diensten wie `WWW` (Wikipedia, «WWW», 2020), E-Mail (Wikipedia, «E-Mail», 2020) oder FTP (Wikipedia, «File Transfer Protocol», 2020) – meist auf der Basis von TCP (Wikipedia, «Transmission Control Protocol», 2020) und IP. Der Datenaustausch selbst passiert über normierte Internetprotokolle (Wikipedia, «Internetprotokollfamilie», 2020) wie zum Beispiel HTTP (Wikipedia, «Hypertext Transfer Protocol», 2020).

Das Internet selbst entstand aus dem Arpanet (Wikipedia, «Arpanet», 2020). 

## Domain Name System DNS

Das *Domain Name System DNS* ist einer der wichtigsten Dienste in IP-basierten Netzwerken. Seine Hauptaufgabe besteht darin, Anfragen mit Namensauflösungen zu beantworten, ähnlich wie bei einer Telefonauskunft. Die Anwender kennen in der Regel die Domain, der *DNS*-Server die zugehörige «Anschlussnummer» in Form der IP-Adresse. 

Ein praktisches Beispiel: Im Webbrowser wird die Domain `www.blick.ch` auf gerufen. Diese Anfrage geht vom Computer an den nächsten *DNS*-Server. Dieser kann im lokalen Netzwerk oder ausserhalb des lokalen Netzwerks stehen. Kennt der lokale *DNS*-Server die IP-Adresse von `blick.ch` nicht, fragt er beim nächst übergeordneten *DNS*-Server an. Die Anfrage wird so lange weitergereicht, bis sie beantwortet werden kann. Wenn sie nicht beantwortet werden kann, gibt der Browser eine Fehlermeldung aus.

Die öffentliche IP einer Domain kann zum Beispiel auch über die Kommandozeileneingabe eines Computers aufgelöst werden. Zum Beispiel mit `dig`:

```bash
dig www.blick.ch +short
# ...
104.66.174.233
```

### IP-Adresse

Bei der IP-Adresse ist aktuell der Standard v4 gebräuchlich. Er besteht in der Regel aus 32 Bits respektive vier mal 8 Bits. Das heisst, es sind maximal 2<sup>32</sup> oder 4294967296 Adressen möglich. In der Notation werden die einzelnen Bits durch Punkte voneinander getrennt, zum Beispiel: `192.168.1.1`.

Da es viel mehr IP-fähige Geräte gibt, als IPv4-Adressen möglich sind, wird aktuell weltweit auf den IPv6-Standard umgestellt. Im IPv6 Standard sind viel mehr Adressen möglich als im IPv4-Standard. Technisch gesehen basiert der IPv6-Standard wiederum auf der Aneinanderreihung von Bits. Die Notation für den Menschen ist jedoch neu und wird hexadezimal dargestellt: `2001:db8:85a3::8a2e:370:7344`.

## Domain

Die Domain oder der Domain-Namensraum ist baumförmig in der Struktur. Die «Blätter» und «Knoten» innerhalb dieses Baums werden als Label bezeichnet. Labels sind Zeichenketten, die durch Punkte getrennt werden. Ein Domainname besteht also aus mehreren Labels. Er wird zudem, streng genommen, mit einem Punkt geschlossen – auch wenn dieser, in der Praxis auf der Ebene der Anwendenden, meist weggelassen wird. Rein Formal sieht ein Domainname also so aus: `www.blick.ch.`. In der Praxis ist jedoch `www.blick.ch` ohne Punkt am Schluss gebräuchlich.

Jeder Knoten im Baum besitzt einen Namen. Ohne die vollständige Angabe des gesamten Namens ist er nicht gültig. `blick` lässt sich zum Beispiel nicht auflösen.

Der Name respektive das Label ganz rechts steht in der Hierarchie des Baums ganz oben, das Label ganz links dagegen am niedrigsten.

Die höchste Ebene in der *DNS*-Hierarchie ist das Null- oder Root-Label. Topologisch unterhalb und damit links des Root-Labels folgt der Name der *Top-Level-Domain TLD*. Danach folgen die Namen der *Second-Level-Domains* und dann der *Third-Level-Domains*, oft auch als *Subdomains* benannt

`ch` ist eine *Top-Level-Domain*. `blick` ist eine *Second-Level-Domain* innerhalb des Namensraums von `ch`. *Second-Level-Domains* können wiederum *Subdomains* enthalten, zum Beispiel `www`.

TLSs werden von der *Internet Assigned Numbers Authority IANA* respektive der ICANN verwaltet und vergeben. Übersetzt heisst der Name der Organisation in etwa «Behörde für die zugewiesenen Nummern des Internets» (Wikipedia, «Internet Assigned Numbers Authority», 2019).

![Domain Name Tree](./assets/domain.png "Domain Name Tree")

Das *Domain-Vergabeverfahren* schreibt vor, dass jeder Domain-Name im Internet und weltweit einmalig und eindeutig sein muss. Das heisst zusammengefasst: Es darf weltweit nur einmal den *TLD* `ch` geben. Im *TLD* `ch` darf es nur eine *Second-Level-Domain* `blick` geben – und innerhalb von `blick` nur genau eine Subdomain mit dem Namen `www`.

## E-Mail

Im Workflow der E-Mail-Systeme gibt es zwei grundlegende Servertypen, den Postausgangsserver *Simple Mail Transfer Protocol SMTP* (Wikipedia, «Simple Mail Transfer Protocol», 2020) und den Posteingangsserver. Bei den Posteingangsservern gibt es mehrere Typen. Die älteste und einfachste Form sin *Post Office Protocol POP*-Server (Wikipedia, «Post Office Protocol», 2020). Eine Weiterentwicklung von *POP* sind *Internet Message Access Protocol IMAP*-Server (Wikipedia, «Internet Message Access Protocol», 2020). Im Unterschied zu *POP* unterstützen sie Ordnerstrukturen die sich zwischen Mail-Client bei einem Benutzer und *IMAP*-Server synchronisieren lassen.  Eine spezielle Rolle übernehmen in diesem Kontext *Microsoft Exchange Server* (Wikipedia, «Microsoft Exchange Server», 2020). Die *Microsoft Exchange Server* sind streng genommen nicht nur E-Mail-Server, sondern *Groupware-Server* (Wikipedia, «Groupware», 2020). Sie unterstützen die kollaborative Zusammenarbeit, in dem auch Kalender, Notizen und andere Elemente synchronisiert werden können.

Für das Abrufen von E-Mails wir ein E-Mail-Client benötigt. Zu den bekanntesten gehören Mozilla Thunderbird (Mozilla, «Thunderbird», 2019), Apple Mail (Apple Support, «Support für Mail», 2020) und Microsoft Outlook (Microsoft, «Microsoft Outlook», 2020). Eine Spezialform nimmt GMail von Google ein (Google, «Produktiver arbeiten mit Gmail», 2020). GMail basiert auf IMAP und erreicht durch die Kombination mit anderen Technologien eine ähnliche Funktionalität wie Microsoft Outlook.

![Mail](./assets/mail.png "Mail")

## Feeds

Mit Feeds (dt. Einspeisungen) werden Nachrichten über Netzwerke verteilt. Feeds sind meist im Zusammenhang mit dem Internet zu finden. Es ist aber auch möglich, Feeds in einem internen Netzwerk einzusetzen.

Mit Hilfe von Feeds können Nutzer Nachrichten, Blogs oder Podcasts abonnieren ({cite:ps}`buhler_internet_2019`, S. 24). Sie können dabei Feeds «nur» konsumieren, oder sich auch aktiv informieren lassen, wenn neue Inhalte verfügbar sind (Push-Info). In der Regel ist das abhängig von den Einstellungen im Feedreader. Der Feedreader ist das Stück Software, das für das Abonnieren von Feeds eingesetzt wird. Bekannte Feedreader sind unter anderem Feedly ({cite:ps}`feedly_inc_feedly_nodate`), Tiny Tiny RSS ({cite:ps}`dolgov_tiny_nodate`) und Vienna RSS ({cite:ps}`ramaholimihaso_viennarss_nodate`).

### RSS & ATOM

RSS steht offiziell für Really Simple Syndication (__C__). Oft wird es aber auch mit Rich Site Summary übersetzt. RSS ist ein Standard für die Strukturierung von Feeds. Das heisst, die Daten in einem RSS-Feed werden vom Sender so strukturiert, dass die beim Empfänger korrekt interpretiert werden können. Dafür schreibt der RSS-Standard zum Beispiel drei Elemente vor, die jeder Channel haben muss: `title` mit dem Titel des Channels, `link` mit dem Link auf die HTML Webseite, die den Feed publiziert und `description` mit einer kurzen Beschreibung des Feeds. 

```{figure-md} rsschannel
<img src="./assets/rss.channel.png" alt="RSS Channel Specification W3C" class="">

Screenshot mit definition für Channel aus W3C RSS-Standard (eigene Abbildung)
```

Die eigentlichen Nachrichten werden in Elemente mit dem Namen `item` zusammengefasst. Auch ihre Struktur wird durch den RSS-Standard genau definiert.

```{figure-md} rssitem
<img src="./assets/rss.item.png" alt="RSS Item Documentation W3C" class="">

Screenshot mit Definition von `item` aus W3C RSS-Standard (eigene Abbildung)
```

Neben RSS ist ATOM ({cite:ps}`the_internet_society_atom_2005`) das zweite populäre Format für Feeds. Wie RSS basiert auch ATOM auf der Seitenbeschreibungssprache XML ({cite:ps}`w3c_extensible_nodate`). Im Unterschied zu RSS geht ATOM von Informationslisten aus, die in «entries» unterteilt werden. Jeder dieser «entries» hat ein erweiterbares Set von Metadaten angehängt und einen Titel. Das heisst, ATOM kann zwischen Inhaltsformaten, den so genannten MIME Types, unterscheiden. Dagegen unterstützt RSS ausschliesslich HTML oder reinen Text. ATOM ist jedoch deutlich weniger weit verbreitet als RSS.

## RDF

Das Ressource Description Framework, kurz RDF ({cite:ps}`rdf_working_group_rdf_nodate`), ist ein System zur Beschreibung von Ressourcen. Eine Ressource kann zum Beispiel ein Bild im Internet sein, auf die über eine URL zugegriffen wird. Mit RDF können Metadaten über das gesamte Bild angehängt werden, die zum Beispiel die Art des Bildes, sein Autor und eine Beschreibung zum Bildinhalt haben. RDF geht dabei von einem dreigeteilten Modell aus, dem «Triple». Das heisst, die präsentierten Metadaten bestehen immer aus der Beschreibung der Ressource, den Eigenschaften der Ressource und den Werten dieser Eigenschaften ({cite:ps}`sistig_resource_2009`).

## Suchmaschinen und SEO

Das Internet ist ein Heuhaufen von Informationen. Im Alltag suchen wir aber meistens nach einer bestimmten Information – also so zu sagen nach der Nadel im Heuhaufen. Mit Suchmaschinen kann diese Nadel relativ bequem gefunden werden. Der grösste Suchmaschinenanbieter unserer Zeit ist sicher Google ({cite:ps}`google_google_nodate`). Es gibt aber auch weitere Anbieter wie zum Beispiel Bing ({cite:ps}`microsoft_bing_nodate`) oder Duckduckgo ({cite:ps}`duckduckgo_duckduckgo_nodate`).

### SEO (Search Engine Optimization)

Bei der Suchmaschinenoptimierung werden die Inhalte einer Webseite so aufbereitet, dass sie eine Suchmaschine möglichst gut indexieren und «verstehen» kann. Das Indexieren ist ein Vorgang, bei dem eine Suchmaschine die wichtigsten Informationen aus einer Webseite in ihrem Katalog abspeichert. Von diesem Katalog beziehen die Nutzer dann bei der Suchabfrage ihre Ergebnisse. Je genauer die Informationen einer Webseite den Anforderungen einer Suchmaschine entsprechen, desto weiter vorne wird sie im Index der Suchergebnisse angezeigt.

Suchmaschinenoptimierung kann «aktiv» und «passiv» geschehen. Dienste wie Google durchforsten ständig das Internet und «klappern alle Webseiten» auf der Suche nach Informationen ab. Wenn du eine Webseite hast, die Suchmaschinen gut bedient, musst du dich nicht unbedingt «aktiv» bei der Suchmaschine mit deiner Webseite anmelden. Du kannst also «passiv» bleiben und darauf warten, dass der Google Bot bei dir vorbeischaut. Wenn du dagegen eine Suchmaschine möglichst schnell über den Inhalt deiner Webseite informieren willst, musst du dich «aktiv» darum kümmern und deine Webseite beim betreffenden Dienst anmelden.

### Sitemap

Die Sitemap ist eine XML-Datei, in der du Informationen über deine Webseite speicherst. Diese Datei enthält unter anderem Einträge mit den Links zu den einzelnen Seiten deiner Webseite und wann diese zuletzt aktualisiert wurden. Die Sitemap von [www.hfmt.ch](https://www.hfmt.ch)  ist zum Beispiel über <https://www.hfmt.ch/sitemap.xml> für alle Internetteilnehmer erreichbar. 

```xml
<urlset>
  <url>
    <loc>https://www.hfmt.ch/lehrgang/</loc>
    <lastmod>2021-09-20</lastmod>
    <changefreq>monthly</changefreq>
  </url>
  <!-- Weitere Elemente -->
</urlset>
```
_Ausschnitt aus der Datei `sitemap.xml` von [www.hfmt.ch](https://www.hfmt.ch)_


### Titel und Metadaten/Keyword

Die Suchmaschine liest die Sitemap ein und benutzt sich als «Landkarte» durch deine Webseite. Das heisst, sie ruft jede einzelne Seite deiner Webseite auf und versucht aus dieser den Titel und wichtige Metadaten und Keywords auszulesen. Dazu gehören zum Beispiel Angaben über das Tätigkeitsfeld, den Zweck oder das Interessengebiet der Website oder einer Unterseite.

```html
<meta name="description" content="Kurze Beschreibung der Website/einzelnen Seite" />
<meta name="keywords" content="Schule, lernen, Medien, SFGZ" />
```

_Zwei Beispiele für Metadaten-Tags mit Beschreibung und Schlüsselwörtern (nach {cite:ps}`kersken_it-handbuch_2019`, S. 1028)_

### Google Analytics

Google Analytics ({cite:ps}`google_analysetools_nodate`) ist ein Service von Google, bei dem du deine Webseite bei der Suchmaschine anmelden kannst. Damit informierst du Google aktiv über deine Inhalte und Google indexiert deine Seite schneller, als wenn du «passiv» bleibst und kein Konto bei Google Analytics eröffnest. Google ist natürlich daran interessiert, möglichst schnell möglichst viel über deine Webseite zu wissen, weil ihr Anzeigengeschäft wesentlich davon abhängig ist. Sie verwenden die gewonnenen Informationen für die Anzeige von Werbung in den Google-Suchergebnissen. Wenn also ein Kunden nach deiner Webseite bei Google Sucht und Google weiss, dass du Autos verkaufst, kann Google in den Ergebnissen, wo auch deine Webseite auftaucht, gleich noch Werbung zu Autozubehör und anderen Anbietern anzeigen. Im Gegenzug gibt dir Google mit Google Analytics eine ganze Programmpalette in die Hand, mit der du die eigenen Nutzerzahlen analysieren und deine Webseite für den Suchindex verbessern kannst.

```{figure-md} seogoogleanalytics
<img src="./assets/seo.ga.png" alt="Google Analytics HFMT" class="">

Screenshot (Ausschnitt) Google Analytics für [www.hfmt.ch][home] (eigene Abbildung)
```

### Open Graph

Das Open Graph Protokoll ({cite:ps}`facebook_open_nodate`) wurde entwickelt, damit soziale Kanäle wie Facebook, Instagram oder Twitter deine Inhalte besser darstellen können – zum Beispiel wenn ein Nutzer ein Link auf deine Webseite postet. Die Inhalte für das Open Graph Protokoll musst du in speziellen Tags bei den Metadaten der Seite abspeichern.

```{figure-md} seoog
<img src="./assets/seo.og.png" alt="OG Protocol Exampe" class="">

Screenshot (Ausschnitt) Open Graph Metadata ({cite:ps}`facebook_open_nodate`) in Meta-Tags (eigene Abbildung)
```

### AddThis

AddThis ({cite:ps}`oracle_andor_its_affiliate_get_nodate`) ist ein Dienst, mit dem du das Verteilen deiner Inhalte auf mehrere Social-Media-Kanäle vereinfachen kannst. Die einzelnen Kanäle werden dem Nutzer als Button angezeigt, über den er einen Inhalt direkt bei einem Netzwerk veröffentlichen kann. Laut AddThis wird der Dienst 20 Milliarden mal pro Jahr angezeigt in Webseiten angezeigt. Ob das wirklich so ist, kannst du aber nicht nachvollziehen, da unabhängig Zahlen bisher fehlen.

[home]:https://www.hfmt.ch
