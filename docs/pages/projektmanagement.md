# Projektmanagement

Nach meiner Erfahrung sind die Projekte im Umfeld der ICT meist vielschichtig. Sie bestehen aus vielen Abhängigkeiten, die auch in Bereiche führen, die nicht direkt etwas mit der technischen Umsetzung zu tun haben. Diese Bereiche sind nicht selten die Auftraggeber. Zum Beispiel will die Buchhaltung einer Druckerei eine Schnittstelle zur Software der Zeiterfassung – oder die Redaktion einer Zeitschrift ein Redaktionssystem für die Verwaltung ihrer Inhalte. Ohne zu verstehen, welche Aufgabe die Buchhaltung oder die Redaktion erfüllt haben will, lässt sich das Projekt nicht umsetzen. Das heisst ganz konkret: Wenn ich für die Buchhaltung ein Projekt umsetzen soll, muss ich zwingend verstehen, wie die Buchhaltung funktioniert. Ich muss mir neben dem Wissen über die erforderliche Technik auch zu einem grossen Teil buchhalterisches Fachwissen aneignen.

Die Informatik als Ganzes und mit ihr die ICT sind Querschnittstechnologien (siehe Wikipedia: Seite «Querschnittstechnologie», 2019). Sie sind selbst ein Themengebiet oder Wirtschaftszweig und werden in vielen Themengebieten oder Wirtschaftszweigen genutzt.

Die breite Anwendung in vielen Methodendisziplinen macht es zur anspruchsvollen Herausforderung im Fassen der «richtigen» Theorie. In der Praxis erlebe ich es oft, dass sich die «guten Rezepte» für ein erfolgreiches Projektmanagement nicht, oder nicht vollständig zwischen den Projekten übertragen lassen. Es gibt weder auf der Ebene der Technik, noch im Vorgehen, noch in der Theorie ein richtig oder falsch. Dagegen gibt es eine Reihe von Modellen, die helfen können, ein Projekt erfolgreich durchzuführen. 

Ich werde mich für diesen Abschnitt massgeblich an meinen Erfahrungen aus über 10 Jahren Projektmanagement und Entwicklung orientieren. Sie erläutern eine Auswahl von theoretischen Grundlagen, mit denen ich in der Praxis gute Erfahrungen gemacht habe. Diese theoretischen Grundlagen sind zugespitzt auf die Anwendung in der Medienindustrie und auf die Ebene der Projektleitung im Kontext der freien Anwendung.

Projektmanagement kann ich verschiedenen Ausprägungen stattfinden. Modelle wie Hermes (ISB, 2019) helfen bei der Strukturierung und können genauer und weniger genau ausgelegt werden. Freie Projekte bedienen sich, im Verständnis für diese Dokumentation, bei solchen Modellen. Sie folgen ihnen aber nicht oder nicht zwingend genau. In freien Projekten geht es vielmehr darum, das «Beste» in der Situation einzusetzen, um an ein Ziel zu gelangen.

In der *Deutschen Industrienorm DIN* gibt es, gefühlt, für fast alles eine Definition, so auch für den Begriff des Projektmanagements: «Ein Projekt ist ein Vorhaben, das im Wesentlichen durch Einmaligkeit der Bedingungen in seiner Gesamtheit gekennzeichnet ist, wie z. B. Zielvorgabe, zeitliche, finanzielle, personelle und andere Begrenzungen, Abgrenzung gegenüber anderen Vorhaben und projektspezifische Organisation.» (DIN e. V., 2019)

Gubelmann & Romano (2011, S. 12) nennen dafür gute praktische Beispiele, wovon ich drei besonders hervorheben will: 

> Was bedeutet diese Definition konkret?
>
> * Projekte sind zeitlich begrenzt, einmalig und komplex.
> * Projekte haben ein definiertes Ziel, das möglichst umfassend erreicht werden soll.
> * Projekte bilden temporär eine eigene, in sich geschlossene Organisationseinheit.

In den folgenden Abschnitten werde ich durch eine Auswahl von Strategien und Hilfestellungen gehen, für die bei der erfolgreichen Umsetzung eines Projekts helfen können. Es ist aber ganz wichtig, zu verstehen, dass ich die zwei wichtigsten Faktoren eines Projekts nie werde dokumentieren können! Das sind ***Unsicherheit*** und ***Unwissen***. Die folgenden Abschnitte bieten Hilfestellungen, für das Herunterbrechen von ***Unsicherheit*** und ***Unwissen***. Sie lösen diese selbst aber nicht. Das ist die Aufgabe der Projektleiterin, des Projektleiters.

## Projektattribute

In diesem Kapitel werde ich der grundlegenden Struktur von «Projekte und Projektmanagement» in «ICT-Projektplanung und -Überwachung» (Gubelmann & Romano, 2011) folgen, jedoch mit weniger Tiefe und mit eigenen Interpretationen. Den Verweis auf Gubelmann & Romano verwende ich, weil ich ihre Struktur sehr gut finde und um eine gute Orientierung zu gewährleisten, wenn jemand die Absicht hat, das Buch zu beschaffen.

Gubelmann & Romano definieren für ein Projekt acht Merkmale: *Kultur, Dynamik, Ziele, Risiko, spezifische Organisation, Neuartigkeit, begrenzte Ressourcen* und *Komplexität*. Davon abgeleitet verwenden wir im weiteren Verlauf eine vereinfachte Sichtweise.

Ein Projekt ist in der Regel:

* Einmalig;
* Interdisziplinär;
* Begrenzt in *all* seinen Ressourcen;
* Einer oder mehreren Aufgaben verpflichtet, die in Form von Zielen erreicht werden sollen.

Die Gründe für ein Projekt könnten mannigfaltiger nicht sein. In Bezug auf Projekte aus dem Umfeld der ICT sind das zum Beispiel: 

* Wirtschaftliche oder organisatorische Weiterentwicklung
  * Aufbau einer Support-Organisation
  * Reduktion von Kosten des laufenden ICT Betriebs
  * verbesserte Administration mit besserer Zuweisung zu den Kostenverursacher
  * …
* Einführung oder Ablösung von Hardware und/oder Software;
  * Eine bestimmte Software aus dem Workflow herauslösen und durch ein anderes Produkt ersetzen;
  * Einkaufsstrategie für Hardware entwickeln;
  * Bauliche Massnahmen für eine Erweiterung der Serverlandschaft
  * ...

## Projektphasen

Projekte werden gerne in Phasen unterteilt. Hermes (Seite: «Methodenübersicht», 2020) geht zum Beispiel von den vier Phasen *Initialisierung, Konzept, Realisierung* und *Einführung* aus.

In unserem Kontext folgen wir einer leicht differierenden Unterteilung:

* Initialisierung
* Konzept
* Realisierung
* Nacharbeiten

Sie unterscheidet sich im Wesentlichen in der Definition respektive Abgrenzung der letzten zwei Phasen. In HERMES wird die letzte Phase als *Einführung* verstanden. In meinem Modell nenne ich die letzte Phase *Nacharbeiten*. Das hat mehrere praktische Gründe. 

In meiner Praxis starten Projekte in der Regel mit einem Projektbriefing. Es findet statt, nachdem ein Kunde einen Auftrag erteilt hat oder ein Projektantrag durch eine Exekutive bewilligt wurde. Auf das Briefing folgt die Initialisierung, das Konzept und anschliessend die Realisation. Während der Realisation kommt es meist zu kleinen Verschiebungen gegenüber dem Konzept: «Hier eine Funktion mehr, da ein Gerät weniger, dort ein zusätzlicher Button.» Diese kleinen Änderungen haben meist kleine Implikationen, die im Einzelnen trivial, in der Summe dagegen gewichtig sind. Die Realisation endet in der Regel mit einem «Go-live» Datum. Am «Go-live» Datum ist das System wohl bereit für die Produktion, die vielen kleinen Änderungen sind jedoch dokumentarisch noch nicht nachgeführt.

Einige Tage nach dem «Go-live» findet meist ein Debriefing statt. In diesem werden die letzten Änderungen nochmals besprochen um anschliessend, in den Nacharbeiten, zum Beispiel in die Dokumentation übertragen zu werden.

Der Zweck einer eigenen Betrachtung ist jedoch nicht, HERMES oder andere Modelle in Frage zustellen. Wir folgen hier einer eigenen Betrachtung die weniger detailliert und dadurch einfacher in der Anwendung sein soll. Sie soll ein praktisches Beispiel dafür sein, dass freie Projekte, also Projekte, die nicht in einem vorgegebenen Modell oder Standard durchgeführt werden müssen, ihr eigenes Phasenmodell haben dürfen. Es ist dabei sehr wichtig zu verstehen, dass das Phasenmodell, wie wir es verwenden, ***auch nur ein Vorschlag ist***. Es kann sein, dass sich bei euch in der Praxis abweichende Modelle ergeben. Es ist wichtig, deren Eigenheiten aufzugreifen und zu berücksichtigen.

![Projektphasen](./assets/projektphasen.png "Projektphasen")

## Recherche

Recherchearbeit ist oft über ein ganzes Projekt hinweg wichtig. Sie hat iterierenden Charakter und ist nicht nach der Vor-Phase eines Projektantrags beendet. Es ist deshalb essenziell, dass die Recherche von Anfang an korrekt strukturiert und abgelegt wird – um in einer späteren Phase wieder darauf zugreifen oder aufbauen zu können.

In der Recherche werden Informationen zu einem Thema systematisch zusammengetragen. Ich werde mich in den weiteren Absätzen auf ein Modell von Schmidbauer und Knödler-Bunte (2004, S. 64) berufen, das die Recherche selbst in eine Systematik aus Primär- und Sekundärmodell unterteilt. Schmidbauer und Knödler-Bunte gehen in ihrem Buch «Das Kommunikationskonzept» (2004) eingehend auf die Recherche und die Analyse ein. Wir werden im weiteren Verlauf eine vereinfachte, gemischte Form aus Recherche und Analyse verwenden. Ich werde dazu Teile aus «Das Kommunikationskonzept» in unseren Kontext übertragen und einer Systematik folgen, die an Schmidbauer und Knödler-Bunte anlehnt, jedoch kein Abbild ist.

![Recherchemodell](./assets/recherchemodell.png "Recherchemodell")

In der Sekundär-Recherche geht es um das Sammeln und bewerten von vorhandenem Informationsmaterial. In der Primär-Recherche werden dagegen eigene Informationen gesammelt. Ein plakatives Beispiel für die Sekundär-Recherche ist zum Beispiel das Sammeln von Informationen im Internet in Form von Spezifikationen, Statistiken u. Ä. In der Primären-Recherche werden dagegen zum Beispiel Nutzerumfragen gemacht.

Beide Modelle ziehen sich durch eine Vor-, Haupt- und Nachrecherche. In Anlehnung an Schmidbauer und Knödler-Bunte (2004) lässt sich dieser Ablauf auch in unseren Kontext übertragen.

### Vor dem Projekt

In der Vor-Recherche werden die Markierungspunkte für die spätere Haupt-Recherche setzen. Die Vor-Recherche findet in unserem Modell hauptsächlich vor dem Projektantrag statt. Sie dient dazu, die grundlegenden Informationen für die spätere Argumentation im Projektantrag zusammenzutragen.

### Im Projekt

Der Projektantrag ist bewilligt. Nun geht es darum, die Recherche auszuweiten und so viele Informationen wie nötig zusammenzutragen. Hierbei kann es hilfreich sein, einen Rechercheplan anzulegen. Schmidbauer und Knödler-Bunte (2004, u. A. S. 77) benutzen dazu ein Mindmap, das die Arbeit in Recherchebereiche und anschliessend in Jobs unterteilt. Es können aber auch andere Formen verwendet werden. Wichtig ist, dass die Recherche strukturiert wird.

![Rechercheplan](./assets/rechercheplan.png "Rechercheplan")

Es kann weiterführend sinnvoll sein, für die einzelnen Jobs innerhalb der Recherche ein Zeitbudget zu vergeben, um den Recherche-Aufwand unter Kontrolle zu behalten.

| Job | Stunden | Abgabe | Zuständig | …
|-|-|-|-|-
| Job 1 | 10h | 1. 1. 2030 | ABC | …
| Job 2 | 2h | 1. 1. 2030 | DEF | …
| … | … | … | … | …

«Die Recherche geht nie zu Ende.» (Schmidbauer und Knödler-Bunte, 2004, S. 65). Die Nachrecherche dient dazu, während dem Projekt weiterhin Daten zu sammel und diese strukturiert der Recherche zuzuführen. Zum Beispiel kann es sein, dass jemand aus dem Projektteam zufällig über einen Bug (Programmfehler jeglicher Art) stolpert. Es ist wichtig, dass dieser irgendwo erfasst, und anschliessend, noch während der Garantie, behoben wird.

### Weitere Recherchehilfen

Für alle nachfolgenden Recherchehilfe gilt: nicht nur Anwenden, sondern auch erklären, was in der Anwendung passiert. Es ist essenziell, dass nach jeder Aufstellung erklärt wird – tabellarisch oder in einem Fliesstext – warum, wie, wo, entschieden wurde.

#### Entscheidungsbaum oder Entscheidungsmatrix

![Entscheidungsmatrix](./assets/entscheidungsmatrix.png "Entscheidungsmatrix")

Die Entscheidungsmatrix ist ein einfaches Hilfsmittel, um Varianten – oder ganz generell «Dinge» – miteinander zu vergleichen. Im Beispiel gewinnt die erste Variante gegen alle anderen Varianten. Obwohl die zweite Variante in der ersten Auswahl gegen die dritte Auswahl gewinnt, verliert sie in der zweiten Auswahl wiederum gegen die erste Variante. Daraus lässt sich schliessen, dass die dritte Variante in jedem Fall schlechter ist als die erste Variante, weil sie schlechter ist als die zweite Variante, die schlechter ist als die erste Variante.

#### Gegenüberstellung

In der Systematik dieser Matrix bediene ich mich bei Idee einer Gegenüberstellung, wie sie bei der Ansoff-Matrix, auch Produkt-Markt-Matrix genannt (Wikipedia, Seite «Produkt-Markt-Matrix», 2020) Anwendung findet. Die Ansoff-Matrix ist ein Werkzeug aus dem strategischen Management, bei dem, meist in Bezug auf Wachstumsstrategien, Potenziale und Risiken von vier möglichen Produkt-Markt-Kombinationen ausgewertet werden.

Das erstrebenswerte an der Ansoff-Matrix ist, im Kontext von ICT-Projekten, die Idee einer Gegenüberstellung. Die Restriktion auf vier mögliche Kombinationen lassen wir hierbei aussen vor. Das Aufstellen von Gegenüberstellungen soll helfen, bei verdichteten Fragen mit vielen Ausprägungen auf zwei Achsen zu einer ***nachvollziebaren*** Entscheidung oder Auswertung zu gelangen. Zum Beispiel:

*Legende: Bewertung mit 0–3 Punkten, 0 Punkte = nicht erfüllt bis 3 Punkte, genau erfüllt*

| . | Anforderung 1 | Anforderung 1 | Anforderung 2 | Total
|-|-|-|-|-
| Produkt 1 | 1 | 1 | 1 | **3**
| Produkt 2 | 1 | 2 | 3 | **6**
| Produkt 3 | 2 | 3 | 3 | **8**

Wenn mit Bewertungen wie Punkten gearbeitet wird, müssen diese vorgängig eingeführt werden – zum Beispiel in Form einer Legende.

## Projektantrag

In unserem Modell beginnt ein Projekt, wenn ein Projektantrag angenommen oder verworfen wurde. Den perfekten Projektantrag gibt es wahrscheinlich nicht. Trotzdem gibt es eine Reihe von Anhaltspunkten, die dabei helfen, ein Projektvorschlag auf seine Qualität zu überprüfen und eventuell, vor der Abgabe, noch nachzubessern (angelehnt and Gubelmann & Romano, Abb. [1-2], S. 13).

### Checkliste Projektantrag

| Prüfpunkt | Fragen/Todo |
| :-------- | :---------- |
| Ziele | Wurden konkrete, unmissverständliche Ziele definiert? |
| Vorgaben | <p style="line-height: 1.5rem;">Sind die Vorgaben respektive die Aufgabe realistisch?<br />Ist die Ausgangslage schlüssig erklärt?</p> |
| Zeitbedarf | <p style="line-height: 1.5rem;">Ist der angedachte Zeitplan belastbar? Wurde zum Beispiel <br />nach der ALPEN-Methode (siehe Wikipedia: Seite «ALPEN-Methode», 2019) geplant?</p> |
| Abwicklung | <p style="line-height: 1.5rem;">Ist die Abwicklung neben dem Tagesgeschäft denkbar? Wie wird das Projekt <br />in die Gesamtorganisation eingebettet?</p> |
| Einmaligkeit | Ist das Projekt wirklich einmalig? |
| Kosten | <p style="line-height: 1.5rem;">Gibt es eine grobe Kostenkalkulation und ein grobes Budget<br /> das die wesentlichen Punkte nennt?</p> |
| Kontrolle | Sind Kontrollmechansimen angedacht? Zum Beispiel Ausstiegspunkte, Abbruch wenn … |
| Messgrössen | Wurden quantitative ***und*** qualitative Messwerte für die Ziele festgelegt? |
| Lösung | Hält die vorgeschlagene Lösung den Vorgaben, Zielen und Messgrössen stand? |
| Organisation | Ist eine Projektorganisation angedacht? |

### Struktur eines Projektantrags

Die Voraussetzungen für Projektanträge sind in der Praxis sehr unterschiedlich. Oft gibt es innerhalb der Unternehmung Vorlagen, die zum Beispiel vom Qualitätsmanagement zur Verfügung gestellt werden.

### Vorlage/Beispiel

[Vorlage Projektantrag (Google Doc)](https://docs.google.com/document/d/1RlTnQ6bT4vblja59e_ZiRArgawAHfgN60aC2n_yKUTk/edit?usp=sharing)

## Initialisierung

Während der Initialisierung entsteht in der Regel die Daten für das spätere Konzept zusammengetragen sowie eine Organisationsform für das Projekt aufgezogen. In der Initialisierung geht es vor allem um das Verdichten von Informationen. Das Ziel ist, aus den verdichteten Informationen im nächsten Schritt ein Konzept zu destillieren, das später als Vorlage für die spätere Realisation dienen soll. Während der Initialisierung wird unter anderem:

* Die Projektorganisation erstellt (Planung, Zuständigkeiten usw.)
* Systemanforderungen detailliert recherchiert und evaluidert;
* Detailstudien angefertigt;
* Systemarchitekturen verglichen;
* Prototypen gefertigt.
* …

Die Initialisierung endet in der Regel mit einem Rebriefing in Bezug auf den Projektantrag und die Erwartungen der Auftraggeber. Im Rebriefing geht es darum, das Verständnis des Auftrags und der Projektziele zu reflektieren und in Ansätzen eine Lösung oder einen Lösungsweg vorzuschlagen. 

### Varianten

Zu diesem Zeitpunkt ist es oft hilfreich, noch nicht in fertigen Lösungen zu denken. Stattdessen gilt es, die gesammelten Daten grob so auszuwerten, dass den Auftraggebern nach Möglichkeit mehrere Varianten vorgeschlagen werden können. Basierend auf der gewählten Variante, werden anschliessend das Konzept und mit ihm die detaillierte Betrachtung auf die Aufgaben und Ziele formuliert (in Anlehnung an Hermes 5, «Phasenmodell und Anforderungen», 2020).

### Recherche

Die Recherche durchläuft auch diese Phase des Projekts. Allerdings sollte in dieser Phase die Hauptrecherche bestritten werden und die Vorrecherche sollte abgeschlossen sein.

## Konzept

In der Konzeptphase wird das Konzept erstellt. Ein Konzept ist ein strategiesches Papier, in welchem die Umsetzungsabsicht festgehalten wird. Im Rahmen des Unterrichts ist das Konzept für die Webseite [www.hfmt.ch](https://www.hfmt.ch) entstanden. Es dient als Beispiel für diesen Abschnitt. Du findest es [hier](https://docs.google.com/document/d/1DlW5uml_ilRhTQCM7VE7P4DHA_zmCZQEmn-uYt0V_H8/edit?usp=sharing).

## Realisation

In der Realisation wird umgesetzt, was im Konzept definiert wurde. In der Regel ist es so, dass in diesem Abschnitt des Projekts die meisten Konflikte auftreten. Der Projektplanung und Leitung kommt deshalb eine besondere Wichtigkeit zu. Ich nutze diesen Abschnitt, um zwei Modelle der Planung etwas genauer zu beleuchten.

### Gantt

Das Gantt-Diagramm, auch Balkenplan genannt, ordnet Aktivitäten in Bezug auf ihre zeitliche Abfolge. Hierbei wird unter anderem nach Meilensteinen, Vorgängen und Abhängigkeiten unterschieden. In der Regel können auch «kritische Pfade» abgebildet werden.In OmniPlan (omni, OmniPlan, 2020) werden kritische Pfade zum Beispiel mit einer roten Umrahmung ausgewiesen. Kritische Pfade sind eine Kette von Aufgaben, deren Zeitplanung die Gesamtprojektdauer massgeblich beeinflusst. Jede dieser Aufgaben ist von der vorhergehenden abhängig und hat **keinen** Zeitpuffer. Verzögert sich zum Beispiel die Implementierung eines zentralen Servers um drei Tage, wird der Endtermin des Projekts um diese Zeitspanne verlängert.

![Gantt](./assets/omni.png "Gantt")

Gantt-Diagramme eignen sich gut für die Übersicht bei längeren und grösseren Projekten.

### Scrum

Scrum ist ein Vorgehensmodell (Wikipedia, «Vorgehensmodell», 2019) das oft für das Produkt- oder Projektmanagement eingesetzt wird. Es wurde ursprünglich für die Softwareentwicklung entwickelt. Scrum besteht nur aus wenigen einfachen Regeln. Es folgt einem inkrementellen Ansatz in dem Aufgaben in möglichst kleine Einheiten aufgeschlüsselt und priorisiert werden.

![Scrum](./assets/scrum.png "Scrum")

Diese Aufgaben werden in Sprints aufgeteilt. Ein Sprint dauert 1-4 Wochen, wobei in der Praxis ein Zyklus von einer Woche oft zu sehr guten Ergebnissen führt. Nach Ablauf des Sprints wird geschaut, welche Aufgaben erledigt werden konnten und welche nicht. Aus dieser Auswertung ergeben sich die Priorisierung und die Aufgaben für den nächsten Sprint. Der Scrum-Master hat in diesem Zusammenhang eine spezielle Aufgabe. Er moderiert und überwacht die Sprint-Sitzungen der Teams. Wenn es mehrere Scrum-Teams gibt, hält er diese zusammen, respektive behält er den Überblick (Henning, o. D.).

### Kanban

Im Kontext von Scrum und der agilen Entwicklung (Wikipedia, «Agile Softwareentwicklung», 2020) werden die Aufgaben oft auf einem Kanban-Board aufgereiht. Kanban ist eine Methode zur Produktionsprozesssteuerung. Sie dient vor allem dazu, die Anzahl parallel laufender Aufgaben zu reduzieren, um so die Durchlaufzeiten zu erhöhen. Anstatt mit allen Aufgaben eines Sprints sofort zu beginnen, werden sie nacheinander abgearbeitet. Kanban und Scrum sind aber grundsätzlich unabhängig von einander und können auch einzeln eingesetzt werden.

Kanban-Boards lassen sich beispielsweise in Form von Burn-Down-Chars auswerten (Wikipedia, «Burn-Down-Chart», 2020). Burn-Down-Charts ermöglichen eine grafische Übersicht für den verbleibenden Aufwand in Relation zur verbleibenden Zeit. Aus dem Verlauf dieses Charts lässt sich schliessen, ob die Arbeit termingerecht abgeschlossen werden kann. Es wird also auf Basis der Historie der erledigten Arbeiten errechnet, ob es realistisch ist, die noch anstehenden Aufgaben termingerecht zu erledigen.

In der Praxis werden Kanban-Boards oft mit den drei Spalten *To Do*, *In progress* und *Done* vorgeschlagen bereitgestellt. In meiner Praxis hat sich allerdings die Aufstellung nach ZenHub (ZenHub, «How the ZenHub Team Uses ZenHub Boards on GitHub», 2020) besser bewährt. ZenHub geht von den Spalten *Icebox*, *Backlog*, *In Progress*, *Review/QA* und *Done* aus. In *Icebox* liegen Aufgaben mit wenig Priorität oder wenn sie, aus irgend einem Grund, in einen anderen Sprint sollen. Im *Backlog* liegen die Aufgaben, die erledigt werden sollten. *In progress* visualisiert die gerade ativen Aufgaben. Aufgaben mit Rückfragen oder mit Feedbackbedarf werden unter *Review/QA* eingeordnet und abgeschlossene Aufgaben am Schluss unter *Done*.

| Icebox | Backlog | In progress | Review/QA | Done 
|--------|---------|-------------|-----------|-----
| Zurückstellen | To do | In Arbeit | Fragen/Antworten | Fertig


## Nacharbeiten

Bei den Nacharbeiten geht es darum, ein Projekt «aufzuräumen». In dieser Phase ist es wichtig, noch offene Punkte aus dem Konzept abzuschliessen, das Projekt zu dokumentieren und, gegebenenfalls mit dem Kunden ein *Service Level Agreement SLA* abzuschliessen. Insbesondere Letzteres ist für die weitere Kundenbeziehung sehr wichtig. Es regelt, wer, welche Zuständigkeiten hat im weiteren Unterhalt der Betrieb von Systemen, die in der Realisation implementiert wurden.
