# Quellen

## Online

Agile Softwareentwicklung. (2020). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Agile_Softwareentwicklung&amp;oldid=195527771>

ALPEN-Methode. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=ALPEN-Methode&amp;oldid=188325342>

Arpanet. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Arpanet&amp;oldid=193631078>

Bibliographisches Institut GmbH, Dudenverlag. (o.&nbsp;J.). *Duden | Informationstechnologie | Rechtschreibung, Bedeutung, Definition, Herkunft*. Duden. Abgerufen 10. Januar 2020, von <https://www.duden.de/rechtschreibung/Informationstechnologie>

*Bundesrat präsentiert neue Regeln für schweizerische Internet-Domains „.ch“ und „.swiss“*. (2014, März 10). MLL-News. <https://www.mll-news.com/bundesrat-praesentiert-neue-regeln-fuer-schweizerische-internet-domains-ch-und-swiss/>

Burn-Down-Chart. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Burn-Down-Chart&amp;oldid=191764386>

*Cache (CPU)*. (o.&nbsp;J.). Abgerufen 10. Januar 2020, von <https://www.cyberport.de/techniklexikon/inhaltsverzeichnis/c/cache-cpu.html>

DIN e. V. (o.&nbsp;J.). *DIN 69901*. Suchergebnisse 69901. Abgerufen 7. Januar 2020, von <https://www.din.de/de/meta/suche/62730!search?query=69901>

E-Mail. (2020). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=E-Mail&amp;oldid=195609928>

File Transfer Protocol. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=File_Transfer_Protocol&amp;oldid=193745802>

*Gmail – kostenloser Speicherplatz und E-Mails von Google*. (o.&nbsp;J.). Gmail&nbsp;– kostenloser Speicherplatz und E-Mails von Google. Abgerufen 11. Januar 2020, von <http://www.google.com/gmail/about/>

Groupware. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Groupware&amp;oldid=194734554>

*Home | METANET - Web. Mail. Server.* (o.&nbsp;J.). Abgerufen 10. Januar 2020, von <https://www.metanet.ch/>

*Hostpoint—Webhosting, Hosting und Domainnamen. Erstklassiger Support*. (o.&nbsp;J.). Hostpoint. Abgerufen 10. Januar 2020, von <https://www.hostpoint.ch/>

*How the ZenHub team uses kanban boards in GitHub – For beginners*. (2014, Oktober 9). Agile Project Management Best Practices &amp; Guides | ZenHub Blog. <https://www.zenhub.com/blog/how-the-zenhub-team-uses-zenhub-boards-on-github/>

Hypertext Transfer Protocol. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Hypertext_Transfer_Protocol&amp;oldid=193020375>

Informatiksteuerungsorgang des Bundes ISB. (o.&nbsp;J.-a). *Hermes*. Abgerufen 7. Januar 2020, von <https://www.hermes.admin.ch/>

Informatiksteuerungsorgang des Bundes ISB. (o.&nbsp;J.-b). *Methodenübersicht*. Abgerufen 7. Januar 2020, von <https://www.hermes.admin.ch/de/projektmanagement/verstehen/ubersicht-hermes/methodenubersicht.html>

Internet Assigned Numbers Authority. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Internet_Assigned_Numbers_Authority&amp;oldid=194568293>

Internet Message Access Protocol. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Internet_Message_Access_Protocol&amp;oldid=185594988>

Internetdienstanbieter. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Internetdienstanbieter&amp;oldid=193831047>

Internetprotokollfamilie. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Internetprotokollfamilie&amp;oldid=194854344>

*Mail&nbsp;– Offizieller Apple&nbsp;Support*. (o.&nbsp;J.). Abgerufen 11. Januar 2020, von <https://support.apple.com/de-ch/mail>

Methode des kritischen Pfades. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Methode_des_kritischen_Pfades&amp;oldid=192755359>

Microsoft Exchange Server. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Microsoft_Exchange_Server&amp;oldid=195009531>

*Microsoft Outlook – E-Mail und Kalender*. (o.&nbsp;J.). Abgerufen 11. Januar 2020, von <https://products.office.com/de-ch/outlook/email-and-calendar-software-microsoft-outlook>

*Phasen und Meilensteine*. (o.&nbsp;J.). Abgerufen 11. Januar 2020, von <https://www.hermes.admin.ch/de/projektmanagement/verstehen/phasen-und-meilensteine.html>

Post Office Protocol. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Post_Office_Protocol&amp;oldid=192955267>

Produkt-Markt-Matrix. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Produkt-Markt-Matrix&amp;oldid=184930148>

*Project Management Software For Professionals—OmniPlan—The Omni Group*. (o.&nbsp;J.). Abgerufen 11. Januar 2020, von <https://www.omnigroup.com/omniplan/>

*Registrare—Internet Domains*. (o.&nbsp;J.-a). Abgerufen 10. Januar 2020, von <https://www.nic.ch/de/registrars/#collapse-7de39e68-4ccb-11e6-b33a-525400a7a801-2>

*Registrare—Internet Domains*. (o.&nbsp;J.-b). Abgerufen 10. Januar 2020, von <https://www.nic.ch/de/registrars/#collapse-7de39e68-4ccb-11e6-b33a-525400a7a801-1>

Schnabel, P. (o.&nbsp;J.). *Logische, primäre, erweiterte Partition / Partitionieren*. Elektronik Kompendium. Abgerufen 10. Januar 2020, von <https://www.elektronik-kompendium.de/sites/com/0705011.htm>

Simple Mail Transfer Protocol. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Simple_Mail_Transfer_Protocol&amp;oldid=190428253>

*Superschnelles SSD-Webhosting aus der Schweiz*. (o.&nbsp;J.). Abgerufen 10. Januar 2020, von <https://www.cyon.ch/?gclid=EAIaIQobChMIneuauov65gIVhoeyCh2eEQkGEAAYASAAEgIogvD_BwE>

*SWITCH*. (o.&nbsp;J.). Abgerufen 10. Januar 2020, von <https://www.switch.ch/de/>

*Thunderbird—E-Mails leicht gemacht.* (o.&nbsp;J.). Thunderbird. Abgerufen 11. Januar 2020, von <https://www.thunderbird.net/de/>

Vorgehensmodell. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Vorgehensmodell&amp;oldid=187069071>

*Was_MachtDerScrumMasterDenGanzenTagArtikelagilereview201501hw.pdf*. (o.&nbsp;J.). Abgerufen 11. Januar 2020, von [https://www.it-agile.de/fileadmin/agile_review/einzelartikel/Was_MachtDerScrumMaster...hw.pdf](https://www.it-agile.de/fileadmin/agile_review/einzelartikel/Was_MachtDerScrumMasterDenGanzenTagArtikelagilereview201501hw.pdf)

Webhosting. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=Webhosting&amp;oldid=194415223>

Wikipedia-Autoren. (2019). Querschnittstechnologie. In *Wikipedia*. Wikipedia, Die freie Enzyklopädie. <https://de.wikipedia.org/w/index.php?title=Querschnittstechnologie&amp;oldid=193351817>

World Wide Web. (2019). In *Wikipedia*. <https://de.wikipedia.org/w/index.php?title=World_Wide_Web&amp;oldid=194759607>

## Bücher

Gubelmann, J., & Romano, R. (2011). *ICT-Projektplanung und -überwachung: Grundlagen zur Initiierung und Steuerung von ICT-Projekten mit Beispielen, Fragen und Antworten*. Compendio Bildungsmedien.

Kersken, S. (2019). *IT-Handbuch für Fachinformatiker: Der Ausbildungsbegleiter; 9. Auflage* (9., aktualisierte und erweiterte Auflage 2019). Rheinwerk Verlag AG.

Schmidbauer, K., & Knödler-Bunte, E. (2004). *Das Kommunikationskonzept: Konzepte entwickeln und präsentieren* (Dt. Erstausg). Univ. Press, UMC.

Tremp, H., & Ruggiero, M. (2011). *Application Engineering: Grundlagen für die objektorientierte Softwareentwicklung mit zahlreichen Beispielen, Aufgaben und Lösungen* (1. Aufl). Compendio Bildungsmedien.
