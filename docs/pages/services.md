# Hosting, Plattformen, Services

```{warning}
Dieser Inhalt ist noch nicht abschliessend redigiert! Er enthält möglicherweise Fehler.
```

Im Internet gibt es für dich und dein Unternehmen eine ganze Reihe von Dienstleistungen, die du nutzen kannst. Wir beginnen mit einem pragmatischen Beispiel: dem Webhosting.

Der Begriff «Host» kommt aus dem Englischen und bedeutet «Gastgeber». In der Informatik werden gerne Unternehmen oder Rechner, die einen Dienst oder mehrere Dienste zur Verfügung stellen, als Hosts bezeichnet.

Die Dienste der Hosts laufen meist auf einem oder mehreren Servern, die permanent über ein Netzwerk und das Internet erreichbar sind. Die Begriffe «Host» und «Server» meinen in unserer Alltagssprache aber oft das Gleiche, nämlich einen Rechner in Server-Bauweise. Insofern verwenden wir hier bei uns den Begriff «Host» immer dann, wenn wir recht unspezifisch über einen Rechner sprechen, der einen Dienst zur Verfügung stellt. Dagegen brauchen wir den Begriff «Server» immer dann, wenn wir etwas Genaues über das Gerät wissen, respektive wenn uns bekannt ist, dass ein Rechner in Server-Bauweise vorliegt oder erforderlich ist.

In den Anfängen der Computernetzwerke mussten alle, die einen Dienst zur Verfügung stellen wollten, einen Computer oder Server kaufen, um ihren Dienst in einem Netzwerk zu veröffentlichen. Das konnte natürlich ganz schnell sehr viele Ressourcen binden. Gute Hardware war und ist teuer, muss betreut werden und das Gleiche gilt auch für die installierten Dienste. Je nach Geschäftsfeld kann das für eine Firma oder Person schnell zum Problem werden. Deshalb sind in diesem Bereiche eine ganze Reihe von Unternehmen entstanden, die sich auf das zur Verfügung Stellen von Infrastruktur und Diensten spezialisiert haben. Nehmen wir einen Schuhmacher als Beispiel: das Kerngeschäft des Schumachers sind die Schuhe, die er macht. Er hat wahrscheinlich keine Zeit, sich um einen Webserver zu kümmern, der seine Webseite ins Internet bringt. Deshalb wendet er sicht an ein Unternehmen, das eine solche Dienstleistung zur Verfügung stellt. Jetzt muss er, im besten Fall, nur noch seine Inhalte in die Webseite laden. Die Seite selbst, die Technik darunter und ein schickes Design wird ihm vom Dienstleister, dem Internet Service Provider, in einem Webhosting zur Verfügung gestellt.

## Was ist ein Internet Service Provider (ISP)?

Der Internet Service Provider (ISP) ist ein Unternehmen, das Rechnerleistung an Kunden vermietet. In unserer Gegenwart verschwimmt der Begriff des ISP aber ein bisschen. Dies, weil grosse Unternehmen wie Microsoft oder Google auch in das Geschäftsfeld eingestiegen sind. Sie bieten ganze Infrastruktur-Cluster an, die weit über die Möglichkeiten herkömmlicher ISP hinausgehen. Hier sprechen wir dann gerne auch von «Cloud-Anbietern».

Gemein haben alle Anbieter in diesem Sektor, dass sie ihre Rechnerleistung an Kunden vermieten. Bei den klassischen ISP sind das in der Regel Produkte für den Internetzugang, E-Mail und Webseiten an ({cite:ps}`wikipedia_community_internetdienstanbieter_2019`). Auch in der Schweiz gibt es verschiedene ISPs. Das sind zum Beispiel Hostpoint ({cite:ps}`noauthor_hostpoint_nodate`), Metanet ({cite:ps}`metanet_ag_home_nodate`) oder Green ({cite:ps}`green_green_nodate`).

Die Produkte für E-Mail und Webseiten werden gerne unter dem Begriff «Webhosting» zusammengefasst. Zu einem Webhosting gehört meist der Zugang zu einem Webspace, einem Webserver und einem Datenbankserver. Auf dem Webserver können zum Beispiel die Dateien und die Programmierung für eine Webseite abgelegt werden, die dann über den Webserver veröffentlicht werden. Wenn die Webseite aus einem CMS besteht, dann kann dieses zusätzlich seine Inhalte auf dem Datenbankserver abspeichern. Beim E-Mail werden meist IMAP- und POP-Dienste angeboten. Der Kunde kann sich E-Mail-Konten anlegen und diese bei Bedarf auch mit seinem CMS oder über seine Webseite nutzen. Ein klassisches Beispiel für die Nutzung mit einem CMS sind die häufig verwendeten Kontaktformulare. Wenn ein Besucher das Kontaktformular ausfüllt, speicher das CSM den gesendeten Inhalt zuerst in der Datenbank ab. Danach nimmt es den Inhalt und schickt damit ein E-Mail an die definierte E-Mail-Adresse.

Die Hostings respektive Webhostings gibt es in vielen Ausprägungen ({cite:ps}`wikipedia_community_webhosting_2019`). Die wichtigsten Angebote für die Schweizer Medienlandschaft sind wahrscheinlich das *Shared Hosting*, die *Virtual Dedicated Server* und das *Dedicated Hosting*.

## Shared Hosting

Beim Shared Hosting werden die Ressourcen aus einem Pool von einem oder mehreren Serven von mehreren Kunden genutzt – daher der Begriff «shared» (engl. für geteilt).

```{figure-md} servicessharedhosting
<img src="./assets/services.sharedhosting.png" alt="Aufbau eines Shared Hostings" class="">

Schematische Darstellung für Shared Hosting – viele Kunden beziehen Leistungen aus einem Ressourcen-Pool und teilen sich einen oder mehrere Server (eigene Abbildung)
```

## Virtual Dedicated Server

Beim Virtual Dedicated Server bekommt der Kunde eine eigene Umgebung in einem virtuellen Server. Die «Service-Ebene» rutscht also so zu sagen um eine Ebene nach unten. Beim Shared Hosting nutzen Kunden gemeinsam ein Set von Dienstleistungen, das sie nicht oder nur minimal beeinflussen können. Das wird dagegen mit dem Virtual Dedicated Server möglich.

Für das initiale Setup gehen die Hosting-Dienstleister oft von ihrer Standardkonfiguration aus, so wie sie sie beim Shared Hosting verweden.  Die Kunden können aber die virtuelle Umgebung bei Bedarf verändern. Auch ist es möglich, die Leistung anzupassen. Das heisst, Kunden können zum Beispiel sagen, wie viel Speicherplatz und Prozessorleistung sie haben wollen. Diese und ähnliche Parameter werden beim Shared Hosting durch den Dienstleister vorgegeben.

```{figure-md} servicesvirtualdedicatedserver
<img src="./assets/services.virtualdedicatedserver.png" alt="Aufbau des Virtual Dedicated Server" class="">

Schematische Darstellung für Virtual Dedicated Server – Kunden können ihre Umgebung individualisieren (orange) oder die Standardkonfiguration in einer eigenen Umgebung, die zum Beispiel mehr Leistung hat als die beim Shared Hosting erhalten würden, nutzen (eigene Abbildung)
```

## Deticated Hosting

Beim Dedicated Hosting erhält ein Kunde einen [physischen Server](physische-server) mit allen Rechten zur Administration. Dieser Server steht meist in einem Rechenzentrum des ISP. Der Kunde kann entscheiden, ober er sich vollständig um den Server kümmen will – oder ob er Teile von Wartung und Unterhalt an den ISP abgibt, zum Beispiel das Backup. Die Dienstleistungen für Wartung und Unterhalt können zwischen den ISP jedoch stark variieren.

Ein Argument für ein Dedicated Hosting kann zum Beispiel eine Web-Applikation sein, die eine starke Internetleitung braucht. In der Regel können Rechenzentren eine Netzwerk-Bandbreite zur Verfügung stellen, die für durchschnittliche Unternehmen unerreichbar ist.

## ISP/Registrar

Die ISPs treten in der Schweiz seit einigen Jahren auch als Registrar auf. Ein Registrar hat die Erlaubnis, Domains zu registrieren und zu verwalten. Der Registrar wurde durch die ICANN ({cite:ps}`internet_corporation_for_assigned_names_and_numbers_homepage_nodate`) oder eine Domain Name Registry akkreditiert und damit befähigt, dass er diese Eintragungen machen darf. In der Schweiz ist SWITCH ({cite:ps}`noauthor_switch_nodate`) für die Administration von `ch` zuständig und akkreditiert die Schweizer Registrare ({cite:ps}`switch_registrare_nodate`).

Beim Lösen einer Domain oder dem Einkauf eines Webhostings ist es wichtig, genau hinzuschauen, was registriert wird. Die Produkte sind bei den ISP oft sehr eng verknüpft miteinander. Das kann dazu führen, dass versehentlich zum Beispiel statt nur einer Domain, eine Domain und ein Webhosting abonniert werden. Früher war das einfacher. SWITCH hatte lange die Erlaubnis, die Domain selbst vergeben, anstatt Registrars zu akkreditieren. Leider ist das seit einer gesetzlichen Änderung ({cite:ps}`buhlmann_bundesrat_2014`) nicht mehr möglich.

## Cloud Cloud Computing

In den Begriffen _Cloud_ und _Cloud Computing_ kommen ganz viele Strömungen der Informatik zusammen. Mit «Cloud» (deu. Wolke) wird gerne die Verfügbarkeit von Millionen von Rechnern in gigantischen Rechenzentren ({cite:ps}`buhler_internet_2019`, S. 30) beschrieben. Sie sind mit dem Internet verbunden und können eine Vielzahl von Aufgaben übernehmen. Wenn nun ein Kunde eine solche Aufgabe beansprucht und etwas in der Cloud berechnen lässt, dann ist das «Cloud Computing». Beim Cloud Computing kannst du Server, Datenspeicher oder Software oft dynamisch einkaufen. Das heisst, dass dir die Dienste minuten- oder leistungsgenau abgerechnet werden ({cite:ps}`kersken_it-handbuch_2019`, S. 871). So bezahlst du nur genau das, was du beansprucht hast.

Die Dienste des Cloud Computing lassen sich grob in 3-4 Kategorien unterteilen (nach {cite:ps}`kersken_it-handbuch_2019`, S. 872):

### Infrastructure as a Service (IaaS)

Bei Iaas stellt der Dienstleister dem Kunden eine Infrastruktur für seine Server bereit. Diese Umgebung ist meist virtualisiert und kann daher schnell und flexibel angepasst werden. Ein gutes Beispiel für IaaS sind die Angebote der Elastic Compute Cloud (EC2) von Amazon Web Services (AWS).

```{figure-md} servicesiaas
<img src="./assets/services.iaas.png" alt="Screenshot AWS EC2 Calculator" class="">

Die zu erwartenden Kosten können bei IaaS oft sehr genau kalkuliert werden. Screenshot des Amazon AWS Pricing Calculator ({cite:ps}`amazon_web_services_inc_aws_nodate`) für EC2 Services (eigene Abbildung).
```

### Platform as a Service (PaaS)

PaaS sind konkrete Dienste, zum Beispiel eine Datenbank- oder Laufzeitumgebung. Kunden können diese Dienste direkt nutzen, ohne dass sie sich Gedanken zur Infrastruktur machen müssen. Ein gutes Beispiel für PaaS ist die Google App Engine. Bei der Google App Engine kannst du zum Beispiel deine App und deine Datenbank laufen lassen, ohne dass du dafür einen Server installieren oder dich um ein Hosting kümmern musst.

```{figure-md} servicespaas
<img src="./assets/services.paas.png" alt="Screenshot Google App Engine" class="">

Screenshot (eigene Abbildung) von Use Case für Laufzeitumgebung der Google App Engine ({cite:ps}`google_app_nodate`)
```

### Software as a Service (SaaS)

Bei SaaS können Kunden eine Software als Dienstleistung beziehen. Die Software läuft dabei teilweise oder vollständig in der Cloud und nicht mehr vor Ort. Ein gutes Beispiel für SaaS ist die Software Notion. Sie wird direkt vom Hersteller über das Internet angeboten und muss nicht vor Ort auf einem Server installiert werden.

```{figure-md} servicessaas
<img src="./assets/services.saas.png" alt="Screenshot Notion.so" class="">

Screenshot der Webseite von Notion ({cite:ps}`notion_labs_inc_notion_nodate`), eine Software für die Zusammenarbeit in Teams, komplett als SaaS (eigene Abbildung).
```

